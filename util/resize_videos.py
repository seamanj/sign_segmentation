

import os
from joblib import Parallel, delayed
import argparse


def resize_360h(
    video_path,
    dest_path
):
    cmd = f"ffmpeg -i {video_path} -y -an -vf scale=-2:360 {dest_path}"
    os.system(cmd)

# ffmpeg -i /vol/research/extol/tmp/Tao/bsl1k/exp1/data/msasl/videos_original/val/R_G5zGVHxkU_000074_000077.mp4 -y -vf scale=-2:360  /vol/research/extol/tmp/Tao/bsl1k/exp1/data/msasl/videos_360h/val/R_G5zGVHxkU_000074_000077.mp4

def resize_360h_25fps(
    video_path,
    dest_path
):
    cmd = f"ffmpeg -i {video_path} -y -an -vf scale=-2:360 -r 25 {dest_path}"  # -2 for even width: https://trac.ffmpeg.org/wiki/Scaling
    os.system(cmd)

def resize_25fps_256x256(
    video_path,
    dest_path
):
    cmd = f"ffmpeg -i {video_path} -y -an -vf scale=256:256 -r 25 {dest_path}"  # -2 for even width: https://trac.ffmpeg.org/wiki/Scaling
    os.system(cmd)

def main(params):


    input_file = params.input_file
    output_root = params.output_root

    file_name = input_file.split('/')[-1]
    if file_name.split('.')[-1] != 'mov' or ('+' in file_name):
        return

    root_dir = "/vol/research/extol/data/BSLCP/Videos"


    output_root_dir = os.path.join(output_root, "videos-resized-25fps-256x256")


    os.makedirs(output_root_dir, exist_ok=True)

    # seqs = [
    #     os.path.join(root, y)
    #     for root, _, files in os.walk(input_dir)
    #     for y in files
    #     if y.split(".")[-1] == "mov" and ("+" not in y)
    # ]

    relative_dir = os.path.relpath(input_file, root_dir)
    output_dir = os.path.join(output_root_dir, *relative_dir.split('/')[:-1])
    os.makedirs(output_dir, exist_ok=True)
    resize(input_file, output_dir)
    print(1)

def resize(seq, output_dir):
    filename = seq.split('/')[-1]

    output_file = os.path.join(output_dir, filename)
    if not os.path.exists(output_file):
        resize_25fps_256x256(seq, output_file)
    else:
        print("{} exists".format(output_file))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_file",
        type=str,
        default="/vol/research/extol/data/BSLCP/Videos/Conversation/Belfast/1+2/BF1c.mov",
    )

    parser.add_argument(
        "--output_root",
        type=str,
        default="/vol/research/extol/tmp/Tao_BSLCP",
    )
    params, _ = parser.parse_known_args()
    main(params)
