import xmltodict
import argparse
import os
import pickle
import gzip
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import pickle
import sys
import json

def make_dir(dir: str) -> str:
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def save_gklz(obj, filename):
    with gzip.open(filename, "wb") as f:
        pickle.dump(obj, f, -1)



def main(params):

    # annotation_file = params.annotation_file
    input_root = params.input_root
    output_root = params.output_root
    # file_name = annotation_file.split("/")[-1]
    # sub_dir = annotation_file.replace(input_root, '')
    # sub_dir = sub_dir.replace(file_name,'')
    czech_root = params.czech_root



    eaf_files = [
        {
            "root": root,
            "seq_name": y.split(".")[0],
            "input_file": os.path.join(root, y)
        }
        for root, _, files in os.walk(input_root)
        for y in files
        if y.split(".")[-1] == "eaf"
    ]

    gloss_map = {}
    for eaf_file in eaf_files:
        annotation_file = eaf_file['input_file']
        print('open annotation file:{}'.format(annotation_file))
        with open(annotation_file) as fd:
            doc = xmltodict.parse(fd.read())["ANNOTATION_DOCUMENT"]

        valid = False
        RH_IDgloss = None
        LH_IDgloss = None
        for tier in doc["TIER"]:
            if tier["@TIER_ID"] == "RH-IDgloss" and "ANNOTATION" in tier:
                valid = True
                RH_IDgloss = tier
            elif tier["@TIER_ID"] == "LH-IDgloss" and "ANNOTATION" in tier:
                valid = True
                LH_IDgloss = tier
        if not valid:
            print('early returned because no RH-IDgloss OR LH-IDgloss info found')
            continue


        valid = False
        header = doc["HEADER"]
        num_MEDIA_DESCRIPTOR = len(header["MEDIA_DESCRIPTOR"])

        if type(header["MEDIA_DESCRIPTOR"]) is not list:# tj : deal with only one MEDIA_DESCRIPTOR element
            media = header["MEDIA_DESCRIPTOR"]
            video_id = (
                media["@MEDIA_URL"]
                    .split("/")[-1]
                    .split(".")[0]
                    .replace("-comp", "")
                    .replace("-c", "")
                    .replace("-Comp", "")
                    .replace("+c", "")
            )
            if "-" not in video_id and (("(" in video_id) or ("+" not in video_id)):
                valid = True
                video_name = video_id
                offset = float(media.get("@TIME_ORIGIN", 0))
        else:
            for media in header["MEDIA_DESCRIPTOR"]:
                video_id = (
                    media["@MEDIA_URL"]
                        .split("/")[-1]
                        .split(".")[0]
                        .replace("-comp", "")
                        .replace("-c", "")
                        .replace("-Comp", "")
                        .replace("+c", "")
                )
                if "-" not in video_id and (("(" in video_id) or ("+" not in video_id)):
                    valid = True
                    video_name = video_id
                    offset = float(media.get("@TIME_ORIGIN", 0))


        if not valid:
            print('early returned because video file not found')
            continue

        time_order = doc["TIME_ORDER"]["TIME_SLOT"]
        time_slots = {
            ts["@TIME_SLOT_ID"]: float(ts["@TIME_VALUE"])
            for ts in time_order
        }

        #
        # # tj : find the czech file
        #
        # # for root, _, files in os.walk(czech_root):
        # #     for name in files:
        # #         print(os.path.join(root, name))
        # # print(1)
        # czech_files = [
        #     {"file_path": os.path.join(root, name)}
        #     for root, _, files in os.walk(czech_root)
        #     for name in files
        #     if name.startswith(video_name)
        # ]
        #
        # assert len(czech_files) == 1, '{} czech_file matched {} found'.format(len(czech_files), video_name)
        # czech_file = czech_files[0]['file_path']
        # print('loading czech file:{}'.format(czech_file))
        #
        #
        # file_name = czech_file.split("/")[-1]
        # sub_dir = czech_file.replace(czech_root, '')
        # sub_dir = sub_dir.replace(file_name,'')
        #
        #
        #
        #
        # czech_data = load_gklz(czech_file)
        # assert czech_data['seq_name'] == video_name
        #
        # num_frame = czech_data['success'].shape[0]
        #
        # skeletor_data = czech_data
        # skeletor_data['RH_IDgloss'] = np.zeros(num_frame, dtype=int)
        # skeletor_data['LH_IDgloss'] = np.zeros(num_frame, dtype=int)


        annotations = []
        if RH_IDgloss is not None:
            if (type(RH_IDgloss["ANNOTATION"]) == list):
                for ann in RH_IDgloss["ANNOTATION"]:
                    start_ms = (
                        time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]]
                        + offset
                    )
                    stop_ms = (
                        time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]]
                        + offset
                    )
                    gloss = ann["ALIGNABLE_ANNOTATION"]["ANNOTATION_VALUE"]

                    if gloss in gloss_map:
                        gloss_map[gloss] += 1
                    else:
                        gloss_map[gloss] = 1

                    start_frame = math.floor(25.0 * (start_ms / 1000.0))
                    stop_frame = math.floor(25.0 * (stop_ms / 1000.0))

                    # skeletor_data['RH_IDgloss'][start_frame:stop_frame] = 1
            else:
                ann =  RH_IDgloss["ANNOTATION"]
                start_ms = (
                        time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]]
                        + offset
                )
                stop_ms = (
                        time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]]
                        + offset
                )
                gloss = ann["ALIGNABLE_ANNOTATION"]["ANNOTATION_VALUE"]

                if gloss in gloss_map:
                    gloss_map[gloss] += 1
                else:
                    gloss_map[gloss] = 1

                start_frame = math.floor(25.0 * (start_ms / 1000.0))
                stop_frame = math.floor(25.0 * (stop_ms / 1000.0))

                # skeletor_data['RH_IDgloss'][start_frame:stop_frame] = 1

        # if LH_IDgloss is not None:
        #     if (type(LH_IDgloss["ANNOTATION"]) == list): # tj : for case there is only one gloss annotation
        #         for ann in LH_IDgloss["ANNOTATION"]:
        #             start_ms = (
        #                     time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]]
        #                     + offset
        #             )
        #             stop_ms = (
        #                     time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]]
        #                     + offset
        #             )
        #             gloss = ann["ALIGNABLE_ANNOTATION"]["ANNOTATION_VALUE"]
        #
        #             if gloss in gloss_map:
        #                 gloss_map[gloss] += 1
        #             else:
        #                 gloss_map[gloss] = 1
        #     else:
        #         ann = LH_IDgloss["ANNOTATION"]
        #         start_ms = (
        #                 time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF1"]]
        #                 + offset
        #         )
        #         stop_ms = (
        #                 time_slots[ann["ALIGNABLE_ANNOTATION"]["@TIME_SLOT_REF2"]]
        #                 + offset
        #         )
        #         gloss = ann["ALIGNABLE_ANNOTATION"]["ANNOTATION_VALUE"]
        #
        #         if gloss in gloss_map:
        #             gloss_map[gloss] += 1
        #         else:
        #             gloss_map[gloss] = 1

    sorted_gloss_map = dict(sorted(gloss_map.items(), key=lambda x: x[1], reverse=True)) # tj : https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
    # sorted_gloss_map.pop(None) # tj : remove None in the dict
    # sorted_gloss_map = {k: i for i, k in enumerate(sorted_gloss_map.keys())} # tj : replace the frequency with index

    f = open('gloss_map_frequency.pckl', 'wb')
    pickle.dump(sorted_gloss_map, f)
    f.close()

    print(1)

def load_data():
    f = open('gloss_map_frequency.pckl', 'rb')
    gloss_map_tmp = pickle.load(f)
    f1 = open('gloss_map_frequency.txt', 'w')
    sys.stdout = f1
    print("{" + "\n".join("{!r}: {!r},".format(k, v+1) for k, v in gloss_map_tmp.items()) + "}")
    f1.close()
    f.close()
    print('1')

def read_gloss_from_txt():
    with open('gloss_map_subset.txt', 'r') as file:
        data = json.load(file)
    print(1)

def save_gloss_as_txt():

    exDict = {'apple':1, 'pear':2, 'banana':3}
    with open('exdict.txt', 'w') as file:
        file.write(json.dumps(exDict, indent=2))  # tj : https://stackoverflow.com/questions/17055117/python-json-dump-append-to-txt-with-each-variable-on-new-line

if __name__ == "__main__":

    read_gloss_from_txt()

    # Assumes they are in the same order
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--annotation_file",
        type=str,
        default="/home/seamanj/tmp/input/BM01F64OHC.eaf",#"/vol/research/extol/data/BSLCP/Annotations.DCAL/Interviews/Glasgow/GW10F55WDI.eaf",
    )
    #/vol/research/extol/data/BSLCP/Annotations.DCAL/Interviews/Belfast/BF01F28WDI.eaf
    #/vol/research/extol/data/BSLCP/Annotations.DCAL/Interviews/Glasgow/GW02M33WHI.eaf

    parser.add_argument(
        "--input_root",
        type=str,
        default="/vol/research/extol/data/BSLCP/Annotations.DCAL/Conversation",#"/vol/research/extol/data/BSLCP/Annotations.DCAL/",#"/home/seamanj/tmp/input"
        help="",
    )
    parser.add_argument(
        "--czech_root",
        type=str,
        default="/vol/research/SignPose/tj/dataset/DCAL_Czech/",
        help="",
    )
    parser.add_argument(
        "--output_root",
        type=str,
        default="/vol/research/SignPose/tj/dataset/DCAL_gloss_binary/",#"/home/seamanj/tmp/output",
    )
    params, _ = parser.parse_known_args()
    main(params)
