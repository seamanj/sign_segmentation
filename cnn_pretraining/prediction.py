# coding: utf-8
"""
This modules holds methods for generating predictions from a model.
"""
import os
from os.path import *
import sys
from typing import List, Optional
from logging import Logger
import numpy as np
from cnn_pretraining.loss import MyLoss

import torch
import torch.nn as nn
from torch.utils.data import Dataset
from cnn_pretraining.model import Model
from cnn_pretraining.data import make_data_loader
from cnn_pretraining.mask import gen_mask, gen_translated_mask, gen_joint_mask_option, gen_valid_mask
from cnn_pretraining.batch_with_mask import BatchWithMask, BatchWithMask, BatchWithMaskValid, BatchWithMaskValidCls, BatchWithMaskCls, MyBatch
from cnn_pretraining.helpers import make_logger, load_config, get_latest_checkpoint,\
    load_checkpoint, make_dir, load_zipped_pickle, save_zipped_pickle, load_openpose_tar_xz
from cnn_pretraining.data import load_data, load_test_data, load_eval_data
from cnn_pretraining.model import build_model
import random
import math
from cnn_pretraining.helpers import overwrite_file, append_file, embed_text, set_seed,embed_2text, f1_score_numpy

from cnn_pretraining.drawing_for_evaluation import draw_openpose, save_original_images
from cnn_pretraining.draw import drawJoints


from cnn_pretraining.loss import calculate_roc, calculate_val
from operator import itemgetter
# pylint: disable=too-many-arguments,too-many-locals,no-member

import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier


def validate(model: Model, data: Dataset,
                     batch_size: int,
                     use_cuda: bool,
                     max_validation_batches: int = 0,
                     loss_function: torch.nn.Module = None,
                     logger=None, # tj : for test
                     stack_output=False
                     ) -> (float, np.array):


    valid_loader=make_data_loader(dataset=data,
                                  batch_size=batch_size,
                                  shuffle=False)

    # disable dropout
    model.eval() # tj : will make the loss different from the training even with the same data
                 # https://stackoverflow.com/questions/60018578/what-does-model-eval-do-in-pytorch
    # don't track gradients during validation
    with torch.no_grad():
        total_loss = 0.

        total_num = 0
        stacked_output = []
        for valid_batch in valid_loader:
            if max_validation_batches != 0 and total_num > max_validation_batches:
                break

            # batch_size = valid_batch['img'].shape[0] # tj : real loaded batch size

            my_batch = MyBatch(valid_batch['img'], valid_batch['openpose'], valid_batch['mask'], use_cuda)


            # run as during training with teacher forcing
            if loss_function is not None:

                batch_loss = model.get_loss_for_mybatch(
                    my_batch, loss_function=loss_function)

                total_loss += batch_loss

                if logger is not None:
                    logger.info("batch %d batch loss: %f\n"%(total_num, batch_loss))

                total_num += 1

                if stack_output:
                    # output = output.reshape(output.size()[0], output.size()[1], -1, 3)
                    stacked_output.append([valid_batch['skeleton'].detach().cpu().numpy(), \
                                           valid_batch['cls'].detach().cpu().numpy(), \
                                           valid_batch['valid'].detach().cpu().numpy(), \
                                           # valid_batch['video_name'], \
                                           # valid_batch['openpose_name'], \
                                           mask.detach().cpu().numpy(), \
                                           skeleton_output.detach().cpu().numpy(), \
                                           cls_output.detach().cpu().numpy(),\
                                           batch_loss.detach().cpu().numpy(),\
                                           masked_cnn_pretraining.detach().cpu().numpy(),
                                           valid_batch['video_1'],
                                           # valid_batch['video_2'],
                                           valid_batch['openpose_1'],
                                           # valid_batch['openpose_2'],
                                           valid_batch['indices_1'],
                                           # valid_batch['indices_2'],
                                           ])
                # = tj : one batch has 32 frames, 32 indices, one video name and one openpose name


        if loss_function is not None:
            # total validation loss
            valid_loss = total_loss / total_num
        else:
            valid_loss = -1

    return valid_loss, stacked_output





def evaluate(cfg_file,
         ckpt:str,
         logger:Logger=None) -> None:
    #abs = abspath(cfg_file) #tj : remember to fill the working directory in the configuration setting
    cfg = load_config(cfg_file)
    set_seed(seed=cfg["training"].get("random_seed", 42))

    result_dir = cfg["testing"]["result_dir"]

    eval_data_path = cfg['data']['eval_data_path']

    if os.path.isdir(eval_data_path):
        sub_dir = eval_data_path.split('/')[-1]
    elif os.path.isfile(eval_data_path):
        sub_dir = '_'.join(eval_data_path.split('/')[-4:-1])

    if logger is None:
        make_dir(result_dir)
        logger = make_logger(abspath(join(result_dir, "eval.log")))

        # when checkpoint is not specified, take oldest from model dir
    if ckpt is None:
        model_dir = cfg["training"]["model_dir"]
        ckpt = get_latest_checkpoint(model_dir)
        logger.info("the lastest checkpoint is: %s" % ckpt)
        if ckpt is None:
            raise FileNotFoundError("No checkpoint found in directory {}."
                                    .format(model_dir))
        try:
            step = ckpt.split(model_dir + "/")[1].split(".ckpt")[0]
        except IndexError:
            step = "best"

    batch_size = cfg["training"].get(
        "eval_batch_size", cfg["training"]["batch_size"])
    use_cuda = cfg["training"].get("use_cuda", False)
    # max_sentence_length = cfg["data"]["max_sentence_length"]

    window_size = cfg['data']['window_size']

    mask_frames = cfg["training"]["mask_frames"]
    eval_mask_frames = cfg["training"]["eval_mask_frames"]
# load the data
    # _, dev_data, test_data = load_data(data_cfg=cfg["data"])

    # test_data = load_test_data(data_cfg=cfg["data"])

    eval_data = load_eval_data(data_cfg=cfg["data"])
    # logger.info("train frames:{}".format(train_data.__len__()))
    #logger.info("dev frames:{}".format(dev_data.__len__()))
    # logger.info("test frames:{}".format(test_data.__len__()))
    logger.info("eval frames:{}".format(eval_data.__len__()))


    data_to_predict = {"eval": eval_data}
    #"train": train_data,  "dev": dev_data, "test": test_data

    # load model state from disk
    model_checkpoint = load_checkpoint(ckpt, use_cuda=use_cuda)

    # build model and load parameters into it
    model = build_model(cfg["model"], eval_data.skeleton_dim)
    # model.load_state_dict(model_checkpoint["model_state"])
    model.embed.load_state_dict(model_checkpoint["embed"])
    model.encoder.load_state_dict(model_checkpoint["encoder"])
    model.skeleton_output_layer.load_state_dict(model_checkpoint["skeleton_output_layer"])
    model.pre_classifier.load_state_dict(model_checkpoint["pre_classifier"])
    model.cls_output_layer.load_state_dict(model_checkpoint["cls_output_layer"])

    if use_cuda:
        model.cuda()

    draw_input = cfg["evaluating"]["draw_input"]
    draw_openpose = cfg['evaluating']['draw_openpose']
    save_original_images = cfg["evaluating"]["save_original_images"]
    draw_curve = cfg["evaluating"]["draw_curve"]
    draw_groundtruth = cfg["evaluating"]["draw_groundtruth"]
    video_root = abspath(cfg['data']['video_root'])
    openpose_root = abspath(cfg['data']['openpose_root'])
    with_confidence = cfg['training']['with_confidence']

    for data_set_name, data_set in data_to_predict.items():
        loss, stacked_output = validate(
            model, data=data_set, batch_size=batch_size,
            use_cuda=use_cuda, loss_function=Skeletor_Loss(),
            window_size=window_size, mask_frames=eval_mask_frames, with_confidence=with_confidence, logger=logger,
            stack_output=True)
        # logger.info("Average batch loss on \'%4s\' dataset: %16.12f ", data_set_name, loss)
        # selected_indices = [i[0] for i in sorted(enumerate(stacked_output), key=lambda x: x[1][3], reverse=True)]
        # logger.info("Max batch loss on \'%4s\' dataset: %16.12f ", data_set_name,
        #             stacked_output[selected_indices[0]][3])
        # logger.info("Min batch loss on \'%4s\' dataset: %16.12f ", data_set_name,
        #             stacked_output[selected_indices[-1]][3])

        # logger.info("silence: %8.6f, onset: %8.6f, offset: %8.6f, signing: %8.6f", jaccard_silence, jaccard_onset,
        #             jaccard_offset, jaccard_signing)

        num_batch_total = len(stacked_output)

        # stacked_output.append([valid_batch['skeleton'].detach().cpu().numpy(), \
        #                        valid_batch['cls'].detach().cpu().numpy(), \
        #                        output.detach().cpu().numpy(), \
        #                        batch_loss.detach().cpu().numpy(), \
        #                        valid_batch['video'],
        #                        valid_batch['openpose'],
        #                        valid_batch['indices']
        #                        ]
        #                       )

        # selected_indices = [ 80]#452,  340,

        from collections import defaultdict
        counter = defaultdict(int)

        joined_skeleton = defaultdict(int)
        joined_cls = defaultdict(int)
        joined_predict = defaultdict(int)

        joined_indices = np.array([], dtype=int)

        cls_pred = {}
        cls_gt = {}


        for index in range(num_batch_total):
            logger.info("selected batch index: %d", index)
            num_example_total = len(stacked_output[index][0])
            for example_index in range(num_example_total):
                logger.info("selected example index: %d", example_index)
                skeleton = stacked_output[index][0][
                    example_index]  # tj : the first 0 means skeleton field, the second 0 means the first frame in one batch
                skeleton = skeleton.reshape(skeleton.shape[0], -1)

                cls = stacked_output[index][1][
                    example_index]  # tj : 1 means indices field, 0 means the first data in the batch

                cls_output = stacked_output[index][3][example_index]

                video_name = stacked_output[index][5][
                    example_index]  # tj : one batch has one openpose name, not one frame has one openpose name

                openpose_name = stacked_output[index][6][
                    example_index]  # tj : one batch has one openpose name, not one frame has one openpose name

                input_name = stacked_output[index][7][example_index]

                indices = stacked_output[index][8][example_index]

                indices_cpu = indices.detach().cpu().numpy()

                joined_indices = np.unique(np.concatenate((joined_indices, indices_cpu)))

                mid_index = window_size // 2



                cls_pred[indices_cpu[mid_index]] = cls_output[1]
                cls_gt[indices_cpu[mid_index]] = cls


                print(1)
        print(1)

        #         joined_indices = np.unique(np.concatenate((joined_indices, indices_cpu)))
        #
        #         for i in range(window_size):
        #             counter[indices_cpu[i]] += 1
        #             joined_predict[indices_cpu[i]] += predict[i]
        #             if indices_cpu[i] not in joined_skeleton:
        #                 joined_skeleton[indices_cpu[i]] = skeleton[i]
        #             if indices_cpu[i] not in joined_cls:
        #                 joined_cls[indices_cpu[i]] = cls[i]
        #
        # for i in joined_indices:
        #     joined_predict[i] /= counter[i]
        #
        # joined_skeleton = np.array(list(joined_skeleton.values()))
        # joined_predict = np.array(list(joined_predict.values()))
        # joined_cls = np.array(list(joined_cls.values()))

        # tj = : calculate the offset

        # joined_predict_tmp = np.argmax(joined_predict, axis=1)
        #
        # onset_map = {}
        # offset_map = {}
        #
        # for i in range(len(joined_predict_tmp) - 1):
        #     if joined_predict_tmp[i] == 0 and joined_predict_tmp[i + 1] == 3:  # tj : onset
        #         idx = np.where(joined_cls == 1)[0]
        #         distance = idx - i
        #         location = np.argmin(abs(distance))
        #         min_distance = distance[location]
        #         if min_distance in onset_map:
        #             onset_map[min_distance] += 1
        #         else:
        #             onset_map[min_distance] = 1
        #         # print("onset :{}, distance to closest gt:{}".format(i, distance[location]))
        #
        #     if joined_predict_tmp[i] == 3 and joined_predict_tmp[i + 1] == 0:  # tj : offset
        #         idx = np.where(joined_cls == 2)[0]
        #         distance = idx - i
        #         location = np.argmin(abs(distance))
        #         min_distance = distance[location]
        #         if min_distance in offset_map:
        #             offset_map[min_distance] += 1
        #         else:
        #             offset_map[min_distance] = 1

                # print("offset:{}, distance to closest gt:{}".format(i, distance[location]))

        # onset_map = dict(sorted(
        #     onset_map.items()))  # tj : sort the dict, https://stackoverflow.com/questions/9001509/how-can-i-sort-a-dictionary-by-key
        # offset_map = dict(sorted(offset_map.items()))
        #
        # x_onset = np.fromiter(onset_map.keys(), dtype=int)  # tj : get numpy array
        # y_onset = np.fromiter(onset_map.values(), dtype=int)
        #
        # xx_onset = np.arange(min(x_onset), max(x_onset) + 1)
        #
        # yy_onset = np.zeros(max(x_onset) - min(x_onset) + 1)
        # yy_onset[x_onset - min(x_onset)] = y_onset
        #
        # x_offset = np.fromiter(offset_map.keys(), dtype=int)  # tj : get numpy array
        # y_offset = np.fromiter(offset_map.values(), dtype=int)
        #
        # xx_offset = np.arange(min(x_offset), max(x_offset) + 1)
        #
        # yy_offset = np.zeros(max(x_offset) - min(x_offset) + 1)
        # yy_offset[x_offset - min(x_offset)] = y_offset

        output_dir = abspath(join(result_dir, data_set_name, sub_dir))
        # image_dir = abspath(join(result_dir, data_set_name, sub_dir, 'all_images'))
        image_name = '{}_{}'.format(str(index), str(example_index))
        # offset_dir = join(output_dir, 'offset')
        make_dir(output_dir)
        # make_dir(image_dir)
        # make_dir(offset_dir)

        # tj : draw the offset
        # plt.figure(figsize=(25.60, 14.40))
        # plt.bar(xx_onset, yy_onset, align='center',
        #         color='k')  # tj : draw bar chart, https://pythonspot.com/matplotlib-bar-chart/
        # plt.xticks(np.arange((int(min(x_onset) / 10) - 1) * 10, (int(max(x_onset) / 10) + 1) * 10,
        #                      10))  # tj : change the tick density:
        # plt.ylim(ymin=0)  # tj : set the bottom of y from 0
        # plt.tight_layout()
        # plt.savefig(os.path.join(offset_dir, 'onset.png'), dpi=100)
        #
        # plt.figure(figsize=(25.60, 14.40))
        # plt.bar(xx_offset, yy_offset, align='center',
        #         color='k')  # tj : draw bar chart, https://pythonspot.com/matplotlib-bar-chart/
        # plt.xticks(np.arange((int(min(x_offset) / 10) - 1) * 10, (int(max(x_offset) / 10) + 1) * 10,
        #                      10))  # tj : change the tick density:
        # plt.ylim(ymin=0)  # tj : set the bottom of y from 0
        # plt.tight_layout()
        # plt.savefig(os.path.join(offset_dir, 'offset.png'), dpi=100)

        print(1)

        draw_start = 1200
        draw_frames = 512
        draw_indices = range(draw_start, draw_start+ draw_frames)
        # # tj = : force to overwrite
        # np.savetxt(join(output_dir, "skeleton.txt"), joined_skeleton[draw_start:draw_start + draw_frames])
        #
        # np.savetxt(join(output_dir, "cls.txt"), joined_cls[draw_start:draw_start + draw_frames], fmt="%5i")
        # np.savetxt(join(output_dir, "predict.txt"), joined_predict[draw_start:draw_start + draw_frames])
        #
        # with open(join(output_dir, 'video_names.txt'), 'w') as f:
        #     f.write('%s\n' % video_name)
        # with open(join(output_dir, 'openpose_names.txt'), 'w') as f:
        #     f.write('%s\n' % openpose_name)
        # np.savetxt(join(output_dir, "indices.txt"), joined_indices[draw_start:draw_start + draw_frames], fmt="%10i")

        # with open(join(output_dir, 'jaccard.txt'), 'w') as f:
        #     f.write("silence: %8.6f, onset: %8.6f, offset: %8.6f, signing: %8.6f" % (jaccard_silence, jaccard_onset,
        #                                                                              jaccard_offset, jaccard_signing))

        if draw_input:
            from cnn_pretraining.draw import drawJoints
            input = load_zipped_pickle(input_name)
            input = input['skeleton']
            input = input.reshape(input.shape[0], -1)
            input = input[draw_indices,:]

            input_x = input[0:input.shape[0], 0:input.shape[1]:3]
            input_y = input[0:input.shape[0], 1:input.shape[1]:3]
            input_z = input[0:input.shape[0], 2:input.shape[1]:3]
            drawJoints(input_x, input_y, input_z, foldName='skeletor', type="openpose", root_dir=output_dir)
        if save_original_images:
            _save_original_images(output_dir, video_root, video_name, draw_indices)
        if draw_openpose:
            _draw_openpose(output_dir, openpose_root, openpose_name, draw_indices)
        if draw_curve:
            from cnn_pretraining.draw import _draw_curve
            _draw_curve(output_dir, cls_pred, draw_indices)
        if draw_groundtruth:
            from cnn_pretraining.draw import _draw_groundtruth
            _draw_groundtruth(output_dir, cls_gt, draw_indices)

        # if draw_czech and save_original_images and draw_openpose and draw_curve:  # tj : merge 4 videos
        if True:
            image_video_name = join(output_dir, 'image', 'image.mp4')
            image_video_name_text = join(output_dir, 'image', 'image_text.mp4')
            openpose_video_name = join(output_dir, 'openpose', 'openpose.mp4')
            openpose_video_name_text = join(output_dir, 'openpose', 'openpose_text.mp4')
            skeletor_video_name = join(output_dir, 'skeletor', 'skeletor.mp4')
            skeletor_video_name_text = join(output_dir, 'skeletor', 'skeletor_text.mp4')
            curve_video_name = join(output_dir, 'curve', 'curve.mp4')
            curve_video_name_text = join(output_dir, 'curve', 'curve_text.mp4')
            groundtruth_video_name = join(output_dir, 'groundtruth', 'groundtruth.mp4')

            merged_2_video_name = join(output_dir, 'merged_2.mp4')
            merged_3_video_name = join(output_dir, 'merged_3.mp4')
            merged_4_video_name = join(output_dir, 'merged_4.mp4')
            merged_5_video_name = join(output_dir, 'merged_5.mp4')

            embed_text(openpose_video_name, openpose_video_name_text, 'OpenPose')
            embed_text(skeletor_video_name, skeletor_video_name_text, 'Skeletor')

            merge_2_command = 'ffmpeg -y -i ' + image_video_name + ' -i ' + openpose_video_name_text + \
                              ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_2_video_name
            os.system(merge_2_command)

            merge_3_command = 'ffmpeg -y -i ' + merged_2_video_name + ' -i ' + skeletor_video_name_text + \
                              ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_3_video_name
            os.system(merge_3_command)
            merge_4_command = 'ffmpeg -y -i ' + merged_3_video_name + ' -i ' + curve_video_name + \
                              ' -filter_complex "vstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_4_video_name
            os.system(merge_4_command)

            merge_5_command = 'ffmpeg -y -i ' + merged_4_video_name + ' -i ' + groundtruth_video_name + \
                              ' -filter_complex "vstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_5_video_name
            os.system(merge_5_command)

            merge_folder = join(output_dir, 'merge')
            make_dir(merge_folder)
            extract_merge_command = 'ffmpeg -start_at_zero -i ' + merged_5_video_name + ' ' + merge_folder + '/%08d.png'
            os.system(extract_merge_command)
            print('1')


def test(cfg_file,
         ckpt:str,
         logger:Logger=None) -> None:


    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    result_dir = cfg["testing"]["result_dir"]

    sub_dir = cfg['data']['test_data_path'].split('/')[-1]

    if logger is None:
        make_dir(result_dir)
        logger = make_logger(abspath(join(result_dir, "test.log")))

    # when checkpoint is not specified, take oldest from model dir
    if ckpt is None:
        model_dir = cfg["training"]["model_dir"]
        ckpt = get_latest_checkpoint(model_dir)
        logger.info("the lastest checkpoint is: %s"%ckpt)
        if ckpt is None:
            raise FileNotFoundError("No checkpoint found in directory {}."
                                    .format(model_dir))
        try:
            step = ckpt.split(model_dir+"/")[1].split(".ckpt")[0]
        except IndexError:
            step = "best"

    batch_size = cfg["training"].get(
        "eval_batch_size", cfg["training"]["batch_size"])
    use_cuda = cfg["testing"]["use_cuda"]
    max_sentence_length = cfg["data"]["max_sentence_length"]
    window_size = cfg["data"]["window_size"]
    mask_frames = cfg["training"]["mask_frames"]
    eval_mask_frames = cfg["training"]["eval_mask_frames"]

    # load the data
    # _, dev_data, test_data = load_data(data_cfg=cfg["data"])
    # assert step == 2, "test on step 1 not supported yet"
    test_data = load_test_data(data_cfg=cfg["data"])
    # logger.info("train frames:{}".format(train_data.__len__()))
    #logger.info("dev frames:{}".format(dev_data.__len__()))
    logger.info("test frames:{}".format(test_data.__len__()))


    data_to_predict = {"test": test_data}
    #"train": train_data,  "dev": dev_data, "test": test_data

    # load model state from disk
    model_checkpoint = load_checkpoint(ckpt, use_cuda=use_cuda)

    # build model and load parameters into it
    model = build_model(cfg["model"], test_data.skeleton_dim)
    # model.load_state_dict(model_checkpoint["model_state"])
    model.embed.load_state_dict(model_checkpoint["embed"])
    model.encoder.load_state_dict(model_checkpoint["encoder"])
    model.skeleton_output_layer.load_state_dict(model_checkpoint["skeleton_output_layer"])
    model.pre_classifier.load_state_dict(model_checkpoint["pre_classifier"])
    model.cls_output_layer.load_state_dict(model_checkpoint["cls_output_layer"])


    if use_cuda:
        model.cuda()

    num_batch = cfg["testing"]["num_batch"]
    num_example = cfg["testing"]["num_example"]
    drawing = cfg["testing"]["drawing"]

    draw_czech = cfg["testing"]["draw_czech"]
    draw_curve = cfg["testing"]["draw_curve"]
    draw_groundtruth = cfg["testing"]["draw_groundtruth"]
    draw_openpose = cfg['testing']['draw_openpose']
    save_original_images = cfg["testing"]["save_original_images"]
    video_root = abspath(cfg['data']['video_root'])
    openpose_root = abspath(cfg['data']['openpose_root'])
    headless = cfg["testing"]["headless"]
    select_criterion = cfg["testing"]["select_criterion"]
    with_confidence = cfg['training']['with_confidence']
    max_validation_batches = cfg['testing']['max_validation_batches']
    for data_set_name, data_set in data_to_predict.items():
        loss, stacked_output, _, _, _ = validate(
           model, data=data_set, batch_size=batch_size, max_validation_batches=max_validation_batches,
           use_cuda=use_cuda, loss_function=Skeletor_Loss(),
           window_size=window_size , mask_frames=eval_mask_frames, with_confidence=with_confidence, logger=logger, stack_output=True)
        logger.info("Average batch loss on \'%4s\' dataset: %16.12f ", data_set_name, loss)
        selected_indices = [i[0] for i in sorted(enumerate(stacked_output), key=lambda x: x[1][6], reverse=True)]
        logger.info("Max batch loss on \'%4s\' dataset: %16.12f ", data_set_name,
                    stacked_output[selected_indices[0]][6])
        logger.info("Min batch loss on \'%4s\' dataset: %16.12f ", data_set_name,
                    stacked_output[selected_indices[-1]][6])

        num_batch_total = len(stacked_output)
        assert num_batch <= num_batch_total, "num_batch should be less than num_batch_total"

        if select_criterion.lower() == "random":
            selected_indices = random.sample(range(num_batch_total), num_batch)
        elif select_criterion.lower() == "decrease":
            # tmp = sorted(stacked_output, key=lambda x:x[3], reverse=True)
            # https://stackoverflow.com/questions/6422700/how-to-get-indices-of-a-sorted-array-in-python
            # tj = : [1] get the stacked_output data itself, [3] get the batch loss for one batch data, [0] get the indices
            selected_indices = [i[0] for i in sorted(enumerate(stacked_output), key=lambda x:x[1][6], reverse=True)]
            selected_indices = selected_indices[:num_batch]
        elif select_criterion.lower() == 'increase':
            selected_indices = [i[0] for i in sorted(enumerate(stacked_output), key=lambda x: x[1][6])]
            selected_indices = selected_indices[:num_batch]


        logger.info("select_criterion: %s", select_criterion.lower())

        # stacked_output.append([valid_batch['skeleton'].detach().cpu().numpy(), \
        #                        valid_batch['cls'].detach().cpu().numpy(), \
        #                        output.detach().cpu().numpy(), \
        #                        batch_loss.detach().cpu().numpy(), \
        #                        valid_batch['video'],
        #                        valid_batch['openpose'],
        #                        valid_batch['indices']
        #                        ])

        # selected_indices = [ 1]#452,  340,

        from collections import defaultdict
        counter = defaultdict(int)

        joined_skeleton = defaultdict(int)
        joined_cls = defaultdict(int)
        joined_predict = defaultdict(int)

        joined_indices = np.array([], dtype=int)


        for index in selected_indices:
            logger.info("selected batch index: %d", index)
            num_example_total = len(stacked_output[index][0])
            assert num_example <= num_example_total, "num_example should be less than num_example_total"
            selected_example_indices = random.sample(range(num_example_total), num_example)
            # selected_example_indices = [0]
            for example_index in selected_example_indices:
                logger.info("selected example index: %d", example_index)

                skeleton = stacked_output[index][0][
                    example_index]  # tj : the first 0 means skeleton field, the second 0 means the first frame in one batch
                skeleton = skeleton.reshape(skeleton.shape[0], -1)

                cls = stacked_output[index][1][
                    example_index]  # tj : 1 means indices field, 0 means the first data in the batch

                valid = stacked_output[index][2][example_index]

                mask = stacked_output[index][3][example_index]

                skeleton_output = stacked_output[index][4][example_index]

                cls_output = stacked_output[index][5][example_index]

                masked_src = stacked_output[index][7][example_index]
                masked_src = masked_cnn_pretraining.reshape(masked_cnn_pretraining.shape[0], -1)


                video_1 = stacked_output[index][8][example_index]

                # video_2 = stacked_output[index][9][example_index]

                openpose_1 = stacked_output[index][9][example_index]

                # openpose_2 = stacked_output[index][11][example_index]

                indices_1 = stacked_output[index][10][example_index]

                # indices_2 = stacked_output[index][13][example_index]

                # tj : solve the interpolation
                interpolated = np.copy(masked_src)
                masked_prev_1 = -1
                masked_next_1 = -1
                masked_prev_2 = -1
                masked_next_2 = -1
                part = 1
                for i in range(len(mask) - 1):
                    if mask[i] == 0 and mask[i+1] == 1:
                        if part == 1:
                            masked_prev_1 = i
                        elif part == 2:
                            masked_prev_2 = i
                    elif mask[i] == 1 and mask[i+1] == 0:
                        if part == 1:
                            masked_next_1 = i + 1
                            part = 2
                        elif part == 2:
                            masked_next_2 = i + 1

                if masked_prev_1 != -1 and masked_next_1 != -1 and masked_prev_1 < masked_next_1:
                    delta = masked_src[masked_next_1, :] - masked_src[masked_prev_1, :]
                    for i in range(masked_prev_1 + 1, masked_next_1):
                        interpolated[i, :] = masked_src[masked_prev_1, :] + float(i - masked_prev_1) / float(
                            masked_next_1 - masked_prev_1) * delta

                if masked_prev_2 != -1 and masked_next_2 != -1 and masked_prev_2 < masked_next_2:
                    delta = masked_src[masked_next_2, :] - masked_src[masked_prev_2, :]
                    for i in range(masked_prev_2 + 1, masked_next_2):
                        interpolated[i, :] = masked_src[masked_prev_2, :] + float(i - masked_prev_2) / float(
                            masked_next_2 - masked_prev_2) * delta



                output_dir = abspath(join(result_dir, data_set_name, sub_dir, str(index), str(example_index)))


                make_dir(output_dir)




                original = skeleton
                original_x = original[0:original.shape[0], 0:original.shape[1]:3]
                original_y = skeleton[0:original.shape[0], 1:original.shape[1]:3]
                original_z = skeleton[0:original.shape[0], 2:original.shape[1]:3]
                drawJoints(original_x, original_y, original_z, foldName='original', type="openpose", root_dir=output_dir, mask=None, valid=valid)

                if save_original_images:
                    _save_original_images(output_dir, video_root, video_1,  indices_1)
                if draw_openpose:
                    _draw_openpose(output_dir, openpose_root, openpose_1, indices_1)

                if draw_curve:
                    from cnn_pretraining.draw import _draw_curve

                    _draw_curve(output_dir, cls_output[1:-1,:], list(set(cls[1:-1])))
                if draw_groundtruth:
                    from cnn_pretraining.draw import _draw_groundtruth
                    _draw_groundtruth(output_dir, cls[1:-1])


                # skeletor = skeleton_output
                # skeletor_x = skeletor[0:skeletor.shape[0], 0:skeletor.shape[1]:3]
                # skeletor_y = skeletor[0:skeletor.shape[0], 1:skeletor.shape[1]:3]
                # skeletor_z = skeletor[0:skeletor.shape[0], 2:skeletor.shape[1]:3]
                # drawJoints(skeletor_x, skeletor_y, skeletor_z, foldName="skeletor", type="openpose", root_dir=output_dir, mask=None, valid=valid)
                #
                # masked_src_x = masked_src[0:masked_cnn_pretraining.shape[0], 0:masked_cnn_pretraining.shape[1]:3]
                # masked_src_y = masked_src[0:masked_cnn_pretraining.shape[0], 1:masked_cnn_pretraining.shape[1]:3]
                # masked_src_z = masked_src[0:masked_cnn_pretraining.shape[0], 2:masked_cnn_pretraining.shape[1]:3]
                # drawJoints(masked_src_x, masked_src_y, masked_src_z, foldName="input", type="openpose",root_dir=output_dir, mask=None, valid=valid)
                #
                # interpolated_x = interpolated[0:interpolated.shape[0], 0:interpolated.shape[1]:3]
                # interpolated_y = interpolated[0:interpolated.shape[0], 1:interpolated.shape[1]:3]
                # interpolated_z = interpolated[0:interpolated.shape[0], 2:interpolated.shape[1]:3]
                # drawJoints(interpolated_x, interpolated_y, interpolated_z, foldName="interpolated", type="openpose", root_dir=output_dir, mask=None, valid=valid)

                if True:
                    image_video_name = join(output_dir, 'image', 'image.mp4')
                    image_video_name_text = join(output_dir, 'image', 'image_text.mp4')
                    openpose_video_name = join(output_dir, 'openpose', 'openpose.mp4')
                    openpose_video_name_text = join(output_dir, 'openpose', 'openpose_text.mp4')
                    original_video_name = join(output_dir, 'original', 'original.mp4')
                    original_video_name_text = join(output_dir, 'original', 'original_text.mp4')
                    curve_video_name = join(output_dir, 'curve', 'curve.mp4')
                    curve_video_name_text = join(output_dir, 'curve', 'curve_text.mp4')
                    groundtruth_video_name = join(output_dir, 'groundtruth', 'groundtruth.mp4')

                    merged_2_video_name = join(output_dir, 'merged_2.mp4')
                    merged_3_video_name = join(output_dir, 'merged_3.mp4')
                    merged_4_video_name = join(output_dir, 'merged_4.mp4')
                    merged_5_video_name = join(output_dir, 'merged_5.mp4')

                    embed_text(openpose_video_name, openpose_video_name_text, 'OpenPose')
                    embed_text(original_video_name, original_video_name_text, 'Original')

                    merge_2_command = 'ffmpeg -y -i ' + image_video_name + ' -i ' + openpose_video_name_text + \
                                      ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_2_video_name
                    os.system(merge_2_command)

                    merge_3_command = 'ffmpeg -y -i ' + merged_2_video_name + ' -i ' + original_video_name_text + \
                                      ' -filter_complex "hstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_3_video_name
                    os.system(merge_3_command)
                    merge_4_command = 'ffmpeg -y -i ' + merged_3_video_name + ' -i ' + curve_video_name + \
                                      ' -filter_complex "vstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_4_video_name
                    os.system(merge_4_command)

                    merge_5_command = 'ffmpeg -y -i ' + merged_4_video_name + ' -i ' + groundtruth_video_name + \
                                      ' -filter_complex "vstack,format=yuv420p" -c:v libx264  -crf 18 -preset veryfast ' + merged_5_video_name
                    os.system(merge_5_command)

                    merge_folder = join(output_dir, 'merge')
                    make_dir(merge_folder)
                    extract_merge_command = 'ffmpeg -start_at_zero -i ' + merged_5_video_name + ' ' + merge_folder + '/%08d.png'
                    os.system(extract_merge_command)
                    print('1')


def _draw_openpose(root_dir:str, openpose_root:str, openpose,  indices):
    from cnn_pretraining.draw import draw2DJoints, drawPlain
    body_mapping_openpose_to_czech = [0, 1, 5, 6, 2, 3]
    #
    # indices = np.loadtxt(join(root_dir, "indices.txt"))
    # with open(join(root_dir,'openpose_names.txt'), 'r') as f:
    #     # https://stackoverflow.com/questions/14676265/how-to-read-a-text-file-into-a-list-or-an-array-with-python
    #     openpose_names = f.read().splitlines()
    #
    # assert len(openpose_names) == 1


    save_folder = os.path.join(root_dir, "openpose")
    make_dir(save_folder)
    prev_openpose_filename = ''
    keypoints = []
    image_index = 0

    openpose_filename = openpose_root + '/' + openpose
    suffix = openpose_filename.split(".")[-1]
    if suffix == 'gklz':
        keypoints = load_zipped_pickle(openpose_filename)
    elif suffix == 'xz':
        keypoints = load_openpose_tar_xz(openpose_filename)


    for i in range(len(indices)):
        index = int(indices[i])
        # openpose_filename = os.path.join(openpose_root, openpose_names[0])


        num_people = len(keypoints[index]['people'])
        frame_data = []
        if num_people == 0:
            drawPlain(foldName='openpose', root_dir=root_dir,  name="%08d.png" % index)
            continue
        elif num_people > 1:
            dist_min = float('inf')
            x_shoulder_center_most_right = keypoints[index]["people"][0]["pose_keypoints_2d"][3]
            idx_people = 0
            for j in range(1, num_people):  # tj : choose the most right people
                x_shoulder_center_curr = keypoints[index]["people"][j]["pose_keypoints_2d"][3]
                if x_shoulder_center_curr > x_shoulder_center_most_right:
                    x_shoulder_center_most_right = x_shoulder_center_curr
                    idx_people = j
        else:
            idx_people = 0


        face_keypoints_2d = keypoints[index]["people"][idx_people]["face_keypoints_2d"]  # poses is 0-based
        face_keypoints_2d = np.reshape(face_keypoints_2d, (-1, 3))
        #
        pose_keypoints_2d = keypoints[index]["people"][idx_people]["pose_keypoints_2d"]
        pose_keypoints_2d = np.reshape(pose_keypoints_2d, (-1, 3))
        #
        hand_right_keypoints_2d = keypoints[index]["people"][idx_people]["hand_right_keypoints_2d"]
        hand_right_keypoints_2d = np.reshape(hand_right_keypoints_2d, (-1, 3))
        #
        hand_left_keypoints_2d = keypoints[index]["people"][idx_people]["hand_left_keypoints_2d"]
        hand_left_keypoints_2d = np.reshape(hand_left_keypoints_2d, (-1, 3))
        #
        singe_person_data = np.concatenate(
            (pose_keypoints_2d[body_mapping_openpose_to_czech].flatten(), hand_right_keypoints_2d.flatten(),
             hand_left_keypoints_2d.flatten()), axis=0)


        X = np.array(singe_person_data).reshape((1, -1)) # tj : turn vertical vector to horizontal vector

        Xx = X[0:X.shape[0], 0:(X.shape[1]):3]  # tj : (583, 50), jump by every 3 elements
        Xy = X[0:X.shape[0], 1:(X.shape[1]):3]  # tj : (583, 50)
        Xw = X[0:X.shape[0], 2:(X.shape[1]):3]  # tj : (583, 50)

        draw2DJoints(Xx, Xy, foldName='openpose', root_dir=root_dir,  name="%08d.png" % image_index)
        image_index += 1



    save_folder = os.path.join(root_dir, "openpose")
    video_name = join(save_folder, 'openpose.mp4')
    gen_video_command = 'ffmpeg' + ' -y -i ' + save_folder + '/%08d.png -c:v libx264 ' + video_name
    print(gen_video_command)
    os.system(gen_video_command)



def _save_original_images(root_dir:str, video_root, video, indices ):
    import cv2
    # indices = np.loadtxt(join(root_dir, "indices.txt"), dtype=int)
    # valid = np.loadtxt(join(root_dir, "valid.txt"), dtype=int)
    # indices = indices_tmp[valid == 1]



    image_index = 0
    save_folder = os.path.join(root_dir, "image")
    make_dir(save_folder)

    video_filename = video_root + video
    vidcap = cv2.VideoCapture(video_filename)
    print('video_filename : {}'.format(video_filename))
    for i in range(len(indices)):
        index = int(indices[i])
        # video_filename = os.path.join(videos_path, video_names[0]) # tj : list only has one element, but we need to index it.


        vidcap.set(cv2.CAP_PROP_POS_FRAMES, index)
        # = tj : 0-based index of the frame to be decoded/captured next.
        success, image = vidcap.read()
        if success:
            output_filename = os.path.join(save_folder, '{:08d}.png'.format(image_index))

            # print('output_filename : {}'.format(output_filename))
            # image_resized = cv2.resize(image, (720, 720))
            # cv2.imwrite(output_filename, image_resized)

            image_padded = cv2.copyMakeBorder(image, 140, 140, 0, 0, cv2.BORDER_CONSTANT,
                                              value=[0, 0, 0])  # tj : 640 * 360 -> 640 * 640


            cv2.imwrite(output_filename, image_padded)
            image_index += 1

        else:
            break


    video_name = join(save_folder, 'image.mp4')
    # gen_video_command = 'ffmpeg' + ' -start_number ' + str(int(indices[0])) + ' -y -i ' + save_folder + '/%08d.png -c:v libx264 ' + video_name
    gen_video_command = 'ffmpeg' + ' -y -i ' + save_folder + '/%08d.png -c:v libx264 ' + video_name
    print(gen_video_command)
    os.system(gen_video_command)



    print('1')



if __name__ == "__main__":
    _save_original_images('/home/seamanj/Workspace/BERT_skeleton/results/EXP2_0_10/test/80',
                          '/home/seamanj/Workspace/BERT_skeleton/data/videos')
