# coding: utf-8
"""
Module to represents whole models
"""

import numpy as np

import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.functional as F

#from cnn_pretraining.initialization import initialize_model
from cnn_pretraining.embeddings import Embeddings
from cnn_pretraining.encoders import Encoder, TransformerEncoder
from cnn_pretraining.batch_with_mask import BatchWithMask, BatchWithMask, BatchWithMaskValid, BatchWithMaskValidCls, BatchWithMaskCls, MyBatch
from cnn_pretraining.initialization import initialize_model
from cnn_pretraining.noise import make_noise
import torch.nn.functional as f

import torchvision
from cnn_pretraining.my_resnet import *


class Model(nn.Module):
    """
    Base Model class
    """

    def __init__(self, num_classes) -> None:

        super(Model, self).__init__()


        self.cnn = resnet18()
        self.pose_regressor = nn.Linear(self.cnn.feature_dim, num_classes)




    # pylint: disable=arguments-differ

    def forward(self, img: Tensor):  # tj : src_mask is for attention score, feature_mask is for masking source feature

        embds = self.cnn(img)
        output = self.pose_regressor(embds)
        return output, embds




    def get_loss_for_mybatch(self, batch: MyBatch, loss_function: nn.Module):

        keypoint_output, _ = self.forward(
            img=batch.img
        )

        batch_loss = loss_function(keypoint_output, batch.openpose, batch.mask)

        return batch_loss





def build_model(cfg: dict, num_classes: int) -> Model:


    model = Model(num_classes=num_classes)

    # custom initialization of model parameters
    initialize_model(model, cfg)

    return model
