import torch
from torch.utils.data import Dataset, DataLoader
import os
from glob import glob
import json
from cnn_pretraining.helpers import *
import cv2
from utils import *

from torch.utils.data.dataloader import default_collate

# tj : to process None in __getitem__ in dataset
# https://discuss.pytorch.org/t/questions-about-dataloader-and-dataset/806/4
def my_collate(batch):
    "Puts each data field into a tensor with outer dimension batch size"
    batch = list(filter(lambda x : x is not None, batch))
    return default_collate(batch)

def make_data_loader(dataset: Dataset,
                     batch_size: int,
                     shuffle: bool = False) -> DataLoader:
    params = {
        'batch_size': batch_size,
        'shuffle': shuffle,
        'num_workers': 4,
        'collate_fn': my_collate}
    return DataLoader(dataset, **params)



def load_train_data(data_cfg: dict) -> (Dataset):  # tj : for czech data
    # = tj : print the train path
    train_video_path = data_cfg.get("train_video_path")
    train_openpose_path = data_cfg.get("train_openpose_path")
    window_size = data_cfg.get("window_size")
    window_step = data_cfg.get("window_step")
    return CNNDataset(train_video_path, train_openpose_path, window_size, window_step)

def load_val_data(data_cfg: dict) -> (Dataset, Dataset):  # tj : for czech data
    # = tj : print the train path
    val_video_path = data_cfg.get("val_video_path")
    val_openpose_path = data_cfg.get("val_openpose_path")
    window_size = data_cfg.get("window_size")
    window_step = data_cfg.get("window_step")
    return CNNDataset(val_video_path, val_openpose_path, window_size, window_step)
def load_test_data(data_cfg: dict) -> (Dataset):  # tj : for czech data
    # = tj : print the train path
    test_path = data_cfg.get("test_data_path")
    video_root = data_cfg.get("video_root")
    openpose_root = data_cfg.get("openpose_root")
    window_size = data_cfg.get("window_size")
    window_step = data_cfg.get("window_step")
    # max_sentence_length = data_cfg.get("max_sentence_length")
    # min_sentence_length = data_cfg.get("min_sentence_length")
    # down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    # window_translated_threshold = data_cfg.get("window_translated_threshold")
    return SkeletonDataset_gloss_multiple(test_path, video_root, openpose_root, window_size, window_step, window_success_threshold)

def load_eval_data(data_cfg: dict) -> (Dataset):  # tj : for czech data
    # = tj : print the train path
    eval_path = data_cfg.get("eval_data_path")
    video_root = data_cfg.get("video_root")
    openpose_root = data_cfg.get("openpose_root")
    window_size = data_cfg.get("eval_window_size")
    window_step = data_cfg.get("eval_window_step")
    # max_sentence_length = data_cfg.get("max_sentence_length")
    # min_sentence_length = data_cfg.get("min_sentence_length")
    # down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    # window_translated_threshold = data_cfg.get("window_translated_threshold")
    return SkeletonDataset_gloss_multiple(eval_path, video_root, openpose_root, window_size, window_step, window_success_threshold)

def load_data(data_cfg: dict) -> (Dataset, Dataset, Dataset):
    # = tj : print the train path
    train_path = data_cfg.get("train_data_path")
    dev_path = data_cfg.get("dev_data_path")
    test_path = data_cfg.get("test_data_path")
    video_root = data_cfg.get("video_root")
    openpose_root = data_cfg.get("openpose_root")
    window_size = data_cfg.get("window_size")
    window_step = data_cfg.get("window_step")
    # max_sentence_length = data_cfg.get("max_sentence_length")
    # min_sentence_length = data_cfg.get("min_sentence_length")
    # down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    # window_translated_threshold = data_cfg.get("window_translated_threshold")

    # print(train_path)
    return SkeletonDataset_gloss_multiple(train_path, video_root, openpose_root, window_size, window_step, window_success_threshold), \
           SkeletonDataset_gloss_multiple(dev_path, video_root, openpose_root, window_size,  window_step, window_success_threshold), \
           SkeletonDataset_gloss_multiple(test_path, video_root, openpose_root, window_size,  window_step, window_success_threshold)




class CNNDataset(Dataset):
    def __init__(self, video_path, openpose_path, window_size, window_step):
        self.video_path = video_path
        self.openpose_path = openpose_path

        self.window_size = window_size
        self.window_step = window_step

        seqs = [
            {
                "root": root,
                "line": int(y.split(".")[0].replace("ln-","")),
                "label": int(y.split(".")[1].replace("lb-","")),
                "text": y.split(".")[2].replace("gl-",""),
                "person": int(y.split(".")[3].replace("pr-","")),
                "video": y.split(".")[4].replace("vd-",""),
                "full_path": os.path.join(root, y),
                "file_name": y[:y.rfind(".mp4")]
            }
            for root, _, files in os.walk(video_path)
            for y in files
            if y.split(".")[-1] == "mp4"
        ]

        self.samples = []
        for seq in seqs:
            print("start process %s" % seq["full_path"])

            vidcap = cv2.VideoCapture(seq["full_path"])
            num_video_frames = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))


            # tj : find the video file
            openpose_files = [
                os.path.join(root, name)
                for root, _, files in os.walk(openpose_path)
                for name in files
                if name == '{}.openpose.tar.xz'.format(seq['file_name'])
            ]

            print('{} openpose file(s) found for {}'.format(len(openpose_files), seq['file_name']))

            if len(openpose_files) != 1:
                print('searching video_file error')
                continue

            keypoints = load_openpose_tar_xz(openpose_files[0])
            num_openpose_frames = len(keypoints)

            if num_video_frames != num_openpose_frames:
                print('video_frames({}) != openpose_frames({}) for sequence: {}'.format(num_video_frames, num_openpose_frames, seq['file_name']))
                continue

            for i in range(num_video_frames):

                num_people = len(keypoints[i]["people"])  # tj : only process images with one person
                if num_people == 1:
                    self.samples.append({'label': seq['label'], 'text': seq['text'], 'person': seq['person'], 'video': seq['full_path'],
                                        'openpose': openpose_files[0], 'idx': i})


            print("finish process %s" % seq["full_path"])


        print("data path '{}' loading finished".format(video_path))
        print(1)


    def __len__(self):
        """
        Return the total number of samples
        """
        return len(self.samples)  # Hard Code

    def __getitem__(self, index):
        """
        Generate one sample of the dataset
        """

        sample = {}

        seq = self.samples[index]


        vidcap = cv2.VideoCapture(seq['video'])
        vidcap.set(cv2.CAP_PROP_POS_FRAMES, seq['idx'])
        success, bgr = vidcap.read()

        if success:
            # BGR (OpenCV) to RGB (Torch)
            rgb = bgr[:, :, [2, 1, 0]]

            chw = np.transpose(rgb, (2, 0, 1))  # C*H*W
            img = to_torch(chw).float()
            if img.max() > 1:
                img /= 255

            sample['img'] = img
        else:
            return None


        keypoints = load_openpose_tar_xz(seq['openpose'])
        face_keypoints_2d = keypoints[seq['idx']]["people"][0]["face_keypoints_2d"]
        face_keypoints_2d = np.reshape(face_keypoints_2d, (-1, 3))

        pose_keypoints_2d = keypoints[seq['idx']]["people"][0]["pose_keypoints_2d"]
        pose_keypoints_2d = np.reshape(pose_keypoints_2d, (-1, 3))

        hand_right_keypoints_2d = keypoints[seq['idx']]["people"][0]["hand_right_keypoints_2d"]
        hand_right_keypoints_2d = np.reshape(hand_right_keypoints_2d, (-1, 3))

        hand_left_keypoints_2d = keypoints[seq['idx']]["people"][0]["hand_left_keypoints_2d"]
        hand_left_keypoints_2d = np.reshape(hand_left_keypoints_2d, (-1, 3))

        openpose = np.concatenate((face_keypoints_2d[:, :2], pose_keypoints_2d[:, :2],
                                   hand_right_keypoints_2d[:, :2], hand_left_keypoints_2d[:, :2]), axis=0)

        openpose = openpose / 224  # tj : normalization

        confidence = np.concatenate((face_keypoints_2d[:, 2], pose_keypoints_2d[:, 2],
                                   hand_right_keypoints_2d[:, 2], hand_left_keypoints_2d[:, 2]), axis=0)

        mask = np.expand_dims(confidence > 0.8, axis=1)
        mask = np.tile(mask, (1,2))



        sample['openpose'] = openpose.flatten()

        sample['mask'] = mask.flatten()


        return sample


