import re
import enchant
import nltk
from DCAL_dict import dcal_dict, unkown_dict
from fixes import parentheses, typos, unknowns, apostrophes, dashes, punctuation

enchant_dict = enchant.Dict("en")
my_dict = dcal_dict + unkown_dict


def my_word_tokenizer(sentence):

    orig = sentence

    # Parentheses fix
    for w, r in parentheses:
        sentence = sentence.replace(w, r)

    # Typo fixes
    for w, r in typos:
        sentence = sentence.replace(w, r)

    # NNP
    # sentence = (
    #    sentence.replace("Butlin's", "Butlins")
    #    .replace("Alzheimer's", "Alzherimers")
    #    .replace("McDonald's", "McDonalds")
    # )

    # Unknown fix
    for w, r in unknowns:
        sentence = sentence.replace(w, r)

    # Apostrophes fix
    for w, r in apostrophes:
        sentence = sentence.replace(w, r)

    # Dash Fixing
    for w, r in dashes:
        sentence = sentence.replace(w, r)

    # Dotting
    for w, r in punctuation:
        sentence = sentence.replace(w, r)

    # Whitespace fixes
    sentence = sentence.strip()
    sentence = re.sub(" +", " ", sentence)

    # ... fix
    sentence = re.sub(r"(?<!\.)\.\.(?!\.)", " ... ", sentence)

    if "-" in sentence:
        print("\n" + orig)
        print(sentence)
        aa = nltk.pos_tag(nltk.tokenize.word_tokenize(sentence))
        print(aa)
        print("")

    # NLTK tokenizer
    sentence = nltk.tokenize.word_tokenize(sentence)

    # tags = nltk.pos_tag(sentence)
    # print(tags)

    # Spell Checker
    ood = [w for w in sentence if not enchant_dict.check(w)]
    ood = [w for w in ood if w not in my_dict]

    # Remove this line after fixing the dashes
    ood = [w for w in ood if w not in ["thought-", "-", "--"]]

    if len(ood) != 0:
        print()
        raise ValueError("Out of Dictionary: %s, original sentence:" % ood, orig)

    # Convert to Lowercase
    sentence = [w.lower() for w in sentence]

    return sentence
