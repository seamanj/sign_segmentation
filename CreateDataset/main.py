import os
import argparse
import pickle
import random
from typing import NamedTuple, List
from collections import Counter
from word_tokenizer import my_word_tokenizer
from dataset_utils import create_dataset, print_dataset, write_dataset, get_idgloss


def main(params):

    input_dir = params.input_dir
    output_dir = params.output_dir

    ann_files = [os.path.join(p, y) for p, _, x in os.walk(input_dir) for y in x]

    annotations = []
    for af in ann_files:
        with open(af, "rb") as f:
            annotations.append(pickle.load(f))

    free_translations = []
    for ann in annotations:

        # Left Handed Check
        if ann.LeftHanded:
            for ft in ann.FreeTrans:
                tmp = ft.lh_idglosses
                ft.lh_idglosses = ft.rh_idglosses
                ft.rh_idglosses = tmp

        for ft in ann.FreeTrans:
            if ft.annotation.annotation_value is not None:
                if not all([idg is None for idg in ft.rh_idglosses]):
                    free_translations.append(ft)

    translations = []
    glosses = []
    for smp in free_translations:
        sentence = my_word_tokenizer(smp.annotation.annotation_value)
        translations.append(sentence)
        glosses.append(get_idgloss(smp.rh_idglosses))

    dataset = create_dataset(translations, glosses, params.seed)
    # print_dataset(dataset)
    # write_dataset(dataset, params.prefix, output_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=str, default=None, help="")
    parser.add_argument("--output_dir", type=str, default="", help="")
    parser.add_argument("--nvis", type=int, default=100, help="")
    parser.add_argument("--ntest", type=int, default=500, help="")
    parser.add_argument("--ndev", type=int, default=500, help="")
    parser.add_argument("--seed", type=int, default=174, help="")
    parser.add_argument("--prefix", type=str, default="dcal.v2", help="")
    params, _ = parser.parse_known_args()
    main(params)
