from typing import NamedTuple, List
from collections import Counter

class SignDatasetPartition(NamedTuple):
    # General
    nSegments: int

    # Spoken Language
    translations: List
    allWords: List
    uniqueWords: List
    singletonWords: List
    wordCounts: Counter
    nWords: int
    nUniqueWords: int
    nSingletonWords: int

    # Sign Language
    glosses: List
    allGlosses: List
    uniqueGlosses: List
    singletonGlosses: List
    glossCounts: Counter
    nGlosses: int
    nUniqueGlosses: int
    nSingletonGlosses: int


class SignDataset(NamedTuple):
    train: SignDatasetPartition
    dev: SignDatasetPartition
    test: SignDatasetPartition


def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))


def unique_oov(train, other):
    return len(other) - len(intersection(train, other))


def oov_count(counter, oov):
    c = 0
    for w in oov:
        c = c + counter[w]
    return c


def npilton_count(counter, n):
    c = 0
    for v in counter.values():
        if v <= n:
            c = c + 1
    return c


def difference(lst1, lst2):
    return list(set(lst1) - set(lst2))


def print_free_trans(smp):

    print(smp.annotation.annotation_value)
    print("\nFree Translation:")
    # print("File: %s" %s)
    print("ID: %s" % smp.annotation.annotation_id)
    print("Value: %s" % smp.annotation.annotation_value)
    print(
        "Start Time: %s \tStop Time: %s"
        % (
            smp.annotation.start_time_slot.time_value,
            smp.annotation.stop_time_slot.time_value,
        )
    )

    print("\n LH_START \t LH_STOP \t LH_AV " "\t RH_START \t RH_STOP \t RH_AV")

    for lhig, rhig in zip(smp.lh_idglosses, smp.rh_idglosses):

        if lhig:
            lhig_tv_start = lhig.annotation.start_time_slot.time_value
            lhig_tv_stop = lhig.annotation.stop_time_slot.time_value
            lhig_av = lhig.annotation.annotation_value
        else:
            lhig_tv_start = lhig_tv_stop = lhig_av = None

        if rhig:
            rhig_tv_start = rhig.annotation.start_time_slot.time_value
            rhig_tv_stop = rhig.annotation.stop_time_slot.time_value
            rhig_av = rhig.annotation.annotation_value

        else:
            rhig_tv_start = rhig_tv_stop = rhig_av = None

        print(
            " %s \t\t %s \t\t %s \t\t %s \t\t %s \t\t %s"
            % (
                lhig_tv_start,
                lhig_tv_stop,
                lhig_av,
                rhig_tv_start,
                rhig_tv_stop,
                rhig_av,
            )
        )


def get_idgloss(idglosses):
    idglosses = [ann for ann in idglosses if ann is not None]
    idglosses = [ann.annotation.annotation_value for ann in idglosses]
    glist = [None]
    for idg in idglosses:
        if idg is not None:  # TODO: Check the parser
            if idg.replace(" ", "") != glist[-1]:
                glist.append(idg.replace(" ", ""))
    return glist[1:]


def split_samples_idx(nsamples, seed=285):

    random.seed(seed)
    rand_idx = random.sample(range(0, nsamples), nsamples)
    num_setaside = params.ntest + params.ndev

    train_idx = rand_idx[0 : nsamples - num_setaside]
    dev_idx = rand_idx[nsamples - num_setaside : nsamples - params.ndev]
    test_idx = rand_idx[nsamples - params.ndev :]

    assert not intersection(train_idx, dev_idx)
    assert not intersection(train_idx, test_idx)
    assert not intersection(dev_idx, test_idx)

    return train_idx, dev_idx, test_idx


def get_tokens(samples):
    tokens = []
    for smp in samples:
        tokens.extend(smp)
    return tokens


def write_vocab(vocab_list, filepath):
    with open(filepath, "w") as f:
        f.write("<unk>\n")
        f.write("<s>\n")
        f.write("</s>\n")
        for l in vocab_list:
            f.write(l + "\n")


def write_annotation(anno_list, filepath):
    with open(filepath, "w") as f:
        for l in anno_list:
            f.write(" ".join(l) + "\n")


def get_partition(translations, glosses, idx):

    translations = [translations[i] for i in idx]
    all_words = get_tokens(translations)
    unique_words = list(set(all_words))
    word_counts = Counter(all_words)
    singleton_words = []
    for key, value in zip(word_counts.keys(), word_counts.values()):
        if value == 1:
            singleton_words.append(key)

    glosses = [glosses[i] for i in idx]
    all_glosses = get_tokens(glosses)
    unique_glosses = list(set(all_glosses))
    gloss_counts = Counter(all_glosses)
    singleton_glosses = []
    for key, value in zip(gloss_counts.keys(), gloss_counts.values()):
        if value == 1:
            singleton_glosses.append(key)

    return SignDatasetPartition(
        nSegments=len(translations),
        translations=translations,
        allWords=all_words,
        uniqueWords=unique_words,
        singletonWords=singleton_words,
        wordCounts=word_counts,
        nWords=len(all_words),
        nUniqueWords=len(unique_words),
        nSingletonWords=len(singleton_words),
        glosses=glosses,
        allGlosses=all_glosses,
        uniqueGlosses=unique_glosses,
        singletonGlosses=singleton_glosses,
        glossCounts=gloss_counts,
        nGlosses=len(all_glosses),
        nUniqueGlosses=len(unique_glosses),
        nSingletonGlosses=len(singleton_glosses),
    )


def create_dataset(translations, glosses, seed):
    train_idx, dev_idx, test_idx = split_samples_idx(len(translations), seed)

    train_set = get_partition(translations, glosses, train_idx)
    dev_set = get_partition(translations, glosses, dev_idx)
    test_set = get_partition(translations, glosses, test_idx)

    return SignDataset(train=train_set, dev=dev_set, test=test_set)


def print_oov_stats(train, other):

    oov_gloss = difference(other.uniqueGlosses, train.uniqueGlosses)
    oov_words = difference(other.uniqueWords, train.uniqueWords)

    print("\nGlosses:")
    print("OOV: %d" % len(oov_gloss))
    print("total OOV: %d " % oov_count(other.glossCounts, oov_gloss))

    print("\nWords:")
    print("OOV: %d" % len(oov_words))
    print("total OOV: %d " % oov_count(other.wordCounts, oov_words))


def print_partition(partition: SignDatasetPartition):

    print("\nGlosses:")
    print("Segments: %d" % partition.nSegments)
    print("Vocab: %d" % partition.nUniqueGlosses)
    print("tot. tokens: %d" % partition.nGlosses)
    print("singleton: %d" % partition.nSingletonGlosses)
    print("tripleton: %d" % npilton_count(partition.glossCounts, 3))
    print("fivelton: %d" % npilton_count(partition.glossCounts, 5))
    print("tenpilton: %d" % npilton_count(partition.glossCounts, 10))

    print("\nWords")
    print("Segments: %d" % partition.nSegments)
    print("Vocab: %d" % partition.nUniqueWords)
    print("tot. tokens: %d" % partition.nWords)
    print("singleton: %d" % partition.nSingletonWords)
    print("tripleton: %d" % npilton_count(partition.wordCounts, 3))
    print("fivelton: %d" % npilton_count(partition.wordCounts, 5))
    print("tenpilton: %d" % npilton_count(partition.wordCounts, 10))


def print_dataset(dataset: SignDataset):

    print("\n\nTrain Set:")
    print_partition(dataset.train)

    print("\n\nDev Set:")
    print_partition(dataset.dev)

    print("\n\nTest Set:")
    print_partition(dataset.test)

    print("\n\nDev OOV:")
    print_oov_stats(dataset.train, dataset.dev)

    print("\n\nTest OOV:")
    print_oov_stats(dataset.train, dataset.test)


def write_dataset(dataset, prefix, output_dir):

    # Translations
    write_vocab(
        dataset.train.uniqueWords, os.path.join(output_dir, prefix + ".vocab.en")
    )
    write_annotation(
        dataset.train.translations, os.path.join(output_dir, prefix + ".train.en")
    )
    write_annotation(
        dataset.dev.translations, os.path.join(output_dir, prefix + ".dev.en")
    )
    write_annotation(
        dataset.test.translations, os.path.join(output_dir, prefix + ".test.en")
    )

    # Glosses
    write_vocab(
        dataset.train.uniqueGlosses, os.path.join(output_dir, prefix + ".vocab.gloss")
    )
    write_annotation(
        dataset.train.glosses, os.path.join(output_dir, prefix + ".train.gloss")
    )
    write_annotation(
        dataset.dev.glosses, os.path.join(output_dir, prefix + ".dev.gloss")
    )
    write_annotation(
        dataset.test.glosses, os.path.join(output_dir, prefix + ".test.gloss")
    )
