import torch
from torch.utils.data import Dataset, DataLoader
import os
from glob import glob
import json
from src.helpers import *
import cv2
from utils import *

from torch.utils.data.dataloader import default_collate
from src.spatial_transforms import *
from src.my_collate import *
from typing import Dict, List, Tuple, Union

# tj : to process None in __getitem__ in dataset
# https://discuss.pytorch.org/t/questions-about-dataloader-and-dataset/806/4
# def my_collate(batch):
#     "Puts each data field into a tensor with outer dimension batch size"
#     batch = list(filter(lambda x : x is not None, batch))
#     return default_collate(batch)

def make_data_loader(dataset: Dataset,
                     batch_size: int,
                     shuffle: bool = False) -> DataLoader:
    params = {
        'batch_size': batch_size,
        'shuffle': shuffle,
        'num_workers': 4,
        'collate_fn': my_collate}
    return DataLoader(dataset, **params)



def load_train_data(data_cfg: dict, spatial_transform, temporal_transform, spatiotemporal_transform) -> (Dataset):  # tj : for czech data
    # = tj : print the train path

    data_path = data_cfg.get("train_data_path")

    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    sample_stride = data_cfg.get("sample_stride")

    local_path = data_cfg.get("local_path")
    window_success_threshold = data_cfg.get("window_success_threshold")
    # window_translated_threshold = data_cfg.get("window_translated_threshold")
    return MyDataset(data_path, 'train', window_size, window_stride, sample_stride, local_path)

def load_dev_data(data_cfg: dict, spatial_transform, temporal_transform, spatiotemporal_transform) -> (Dataset, Dataset):  # tj : for czech data
    # = tj : print the train path

    data_path = data_cfg.get("dev_data_path")

    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    sample_stride = data_cfg.get("sample_stride")
    local_path = data_cfg.get("local_path")

    window_success_threshold = data_cfg.get("window_success_threshold")

    return MyDataset(data_path, 'val', window_size, window_stride, sample_stride, local_path)

def load_test_data(data_cfg: dict) -> (Dataset):  # tj : for czech data
    # = tj : print the train path
    test_path = data_cfg.get("test_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")

    window_success_threshold = data_cfg.get("window_success_threshold")

    return MyDataset(test_path, window_size, window_stride)

def load_eval_data(data_cfg: dict) -> (Dataset):  # tj : for czech data
    # = tj : print the train path
    eval_path = data_cfg.get("eval_data_path")
    window_size = data_cfg.get("window_size")
    window_stride = data_cfg.get("window_stride")
    # max_sentence_length = data_cfg.get("max_sentence_length")
    # min_sentence_length = data_cfg.get("min_sentence_length")
    # down_sample_rate = data_cfg.get("down_sample_rate")
    window_success_threshold = data_cfg.get("window_success_threshold")
    # window_translated_threshold = data_cfg.get("window_translated_threshold")
    return MyDataset(eval_path,  window_size, window_stride)



class VideoRecord(object):
    def __init__(self, row):
        self._data = row

    @property
    def path(self):
        return self._data[0]

    @property
    def num_frames(self):
        return int(self._data[1])

    @property
    def label(self):
        return int(self._data[2])

def pil_loader(path, format):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert(format)


def accimage_loader(path):
    try:
        import accimage
        return accimage.Image(path)
    except IOError:
        # Potentially a decoding problem, fall back to PIL.Image
        return pil_loader(path)

def get_default_image_loader():
    # from torchvision import get_image_backend
    # if get_image_backend() == 'accimage':
    #     return accimage_loader
    # else:
        return pil_loader

def video_loader(video_dir_path, image_tmpl, format, frame_indices, image_loader):
    video = []
    for i in frame_indices:
        image_path = os.path.join(video_dir_path, image_tmpl.format(i))
        if os.path.exists(image_path):
            video.append(image_loader(image_path, format))
        else:
            return video

    return video

def get_default_video_loader():
    image_loader = get_default_image_loader()
    return functools.partial(video_loader, image_loader=image_loader)


import pandas as pd
def parse_annotations(input_json: Path) -> pd.DataFrame:
    """Returns a parsed DataFrame.

    arguments:
    ---------
    input_json: path to JSON file containing the following columns:
               'YouTube Identifier,Start time,End time,Class label'

    returns:
    -------
    dataset: DataFrame
        Pandas with the following columns:
            'video-id', 'start-time', 'end-time', 'label-name'
    """
    df = pd.read_json(input_json)
    youtube_ids = []
    for i in range(len(df)):
        start_ix = df["url"][i].index("watch?v=") + len("watch?v=")
        end_ix = start_ix + 11
        youtube_ids.append(df["url"][i][start_ix:end_ix])

    df.insert(0, "video-id", youtube_ids)
    df.rename(columns={"start_time": "start-time", "end_time": "end-time"}, inplace=True)
    return df

def construct_video_filename(output_dir, row, trim_format="%06d"):
    """Given a dataset row, this function constructs the
       output filename for a given video.
    """
    # output_filename = "%s_%s_%s.mp4" % (
    #     row["video-id"],
    #     trim_format % row["start-time"],
    #     trim_format % row["end-time"],
    # )

    output_filename = "%s_%s_%s_%s.mp4" % (
        row["video-id"],
        trim_format % row["start-time"],
        trim_format % row["end-time"],
        "%.3f" % (row["end-time"] - row["start-time"])
    )
    return os.path.join(output_dir, output_filename)

class MyDataset(Dataset):
    def __init__(self, data_path, set_name, window_size, window_stride, sample_stride, local_path):

        self.set_name = set_name
        self.window_size = window_size
        self.window_stride = window_stride
        self.sample_stride = sample_stride
        self.local_path = local_path

        # rgb_mean = [0.485, 0.456, 0.406]
        # rgb_std = [0.229, 0.224, 0.225]

        rgb_mean = [0.5, 0.5, 0.5]
        rgb_std = [1, 1, 1]

        self.rgb_normalize = Normalize(rgb_mean, rgb_std)

        flow_mean = [0.5]
        flow_std = [np.mean(rgb_std)]

        self.flow_normalizae = GroupNormalize(flow_mean, flow_std)
        if local_path:
            json_root = "/home/seamanj/Workspace/bsl1k/data/msasl/info"
        else:
            json_root = "/vol/research/extol/tmp/Tao/bsl1k/exp1/data/msasl/info"
        if set_name == 'train':
            input_json = os.path.join(json_root, "MSASL_train.json")
        elif set_name == 'val':
            input_json = os.path.join(json_root, "MSASL_val.json")

        dataset = parse_annotations(input_json)
        trim_format = "%06d"
        self.samples = []
        for i, row in dataset.iterrows():
            video_path = construct_video_filename(data_path, row, trim_format)
            if not os.path.exists(video_path):
                continue
            self.samples.append({'label': row["label"], 'full_path': video_path, 'box': np.asarray(row['box']).astype(np.float32)})


        with open(os.path.join(json_root, "words.txt"), "r") as f:
            self.class_names = f.read().splitlines()
        print("data path '{}' loading finished".format(data_path))
        print(1)

    def gpu_collater(self, minibatch, concat_datasets=None):
        rgb = minibatch["input"]
        assert rgb.is_cuda, "expected tensor to be on the GPU"
        if self.set_name == "train":
            is_hflip = random.random() < 0.5
            if is_hflip:
                # horizontal axis is last
                rgb = torch.flip(rgb, dims=[-1])

        if self.set_name == "train":
            rgb = im_color_jitter(rgb, num_in_frames=self.window_size, thr=0.2)

        # For now, mimic the original pipeline.  If it's still a bottleneck, we should
        # collapse the cropping, resizing etc. logic into a single sampling grid.
        iB, iC, iK, iH, iW = rgb.shape
        assert iK == self.window_size, "unexpected number of frames per clip"

        bbox_yxyx = minibatch["box"].numpy()

        # bbox_yxyx = np.zeros((iB, 4), dtype=np.float32)
        # for ii, data_box in enumerate(minibatch["box"]):
        #     bbox_yxyx[ii] = np.array([0, 0, 1, 1])
        #     # Otherwise, it fails when mixing use_bbox True and False for two datasets
        #     if concat_datasets is not None:
        #         local_use_bbox = concat_datasets[minibatch["dataset"][ii]].use_bbox
        #     else:
        #         local_use_bbox = self.use_bbox
        #     if local_use_bbox:
        #         # Until we patch ConcatDataset, we need to pass the dataset object
        #         # explicitly to handle bbox selection
        #         if concat_datasets is not None:
        #             get_bbox = concat_datasets[minibatch["dataset"][ii]]._get_bbox
        #         else:
        #             get_bbox = self._get_bbox
        #         bbox_yxyx[ii] = get_bbox(data_index)



        # require that the original boxes lie inside the image
        bbox_yxyx[:, :2] = np.maximum(0, bbox_yxyx[:, :2])
        bbox_yxyx[:, 2:] = np.minimum(1, bbox_yxyx[:, 2:])

        self.scale_factor = 0.1
        self.inp_res = 224
        self.resize_res = 256
        self.mean = 0.5 * torch.ones(3)
        self.std = 1.0 * torch.ones(3)



        if self.set_name == "train":
            if is_hflip:
                flipped_xmin = 1 - bbox_yxyx[:, 3]
                flipped_xmax = 1 - bbox_yxyx[:, 1]
                bbox_yxyx[:, 1] = flipped_xmin
                bbox_yxyx[:, 3] = flipped_xmax

            # apply a random (isotropic) scale factor to box coordinates
            rand_scale = np.random.rand(iB, 1)
            rand_scale = 1 - self.scale_factor + 2 * self.scale_factor * rand_scale
            # Mimic the meaning of scale used in CPU pipeline
            rand_scale = 1 / rand_scale
            bbox_yxyx = scale_yxyx_bbox(bbox_yxyx, scale=rand_scale)

        # apply random/center cropping to match the proportions used in the original code
        # (the scaling is not quite identical, but close to it)
        if self.set_name == "train":
            crop_box_sc = (self.inp_res / self.resize_res) * rand_scale
        else:
            crop_box_sc = self.inp_res / self.resize_res
        bbox_yxyx = scale_yxyx_bbox(bbox_yxyx, scale=crop_box_sc)

        # If training, jitter the location such that it still lies within the appropriate
        # region defined by the (optionally scaled) bounding box
        if self.set_name == "train":
            crop_bbox_cenhw = bbox_format(bbox_yxyx, src="yxyx", dest="cenhw")
            cropped_hw = crop_bbox_cenhw[:, 2:]
            valid_offset_region_hw = ((1 - crop_box_sc) / crop_box_sc) * cropped_hw
            valid_offset_samples = np.random.rand(iB, 2)
            valid_rand_offsets = (valid_offset_samples - 0.5) * valid_offset_region_hw
            # apply offsets
            bbox_yxyx += np.tile(valid_rand_offsets, (1, 2))

        # TODO(Samuel): go back over:
        #  (1) the corner alignment logic to check we are doing # the right thing here.
        #  (2) whether zero padding is appropriate for out-of-bounds handling
        # center in [-1, -1] coordinates
        bbox_yxyx = 2 * bbox_yxyx - 1
        grids = torch.zeros(
            iB, self.inp_res, self.inp_res, 2, device=rgb.device, dtype=rgb.dtype
        )

        for ii, bbox in enumerate(bbox_yxyx):
            yticks = torch.linspace(start=bbox[0], end=bbox[2], steps=self.inp_res)
            xticks = torch.linspace(start=bbox[1], end=bbox[3], steps=self.inp_res)
            grid_y, grid_x = torch.meshgrid(yticks, xticks)
            # The grid expects the ordering to be x then y
            grids[ii] = torch.stack((grid_x, grid_y), 2)

        # merge RGB and clip dimensions to use with grid sampler
        rgb = rgb.view(rgb.shape[0], 3 * self.window_size, iH, iW)
        rgb = torch.nn.functional.grid_sample(
            rgb, grid=grids, mode="bilinear", align_corners=False, padding_mode="zeros",
        )
        # unflatten channel/clip dimension
        rgb = rgb.view(rgb.shape[0], 3, self.window_size, self.inp_res, self.inp_res)
        rgb = color_normalize(rgb, self.mean, self.std)
        minibatch["input"] = rgb
        return minibatch


    def __len__(self):
        """
        Return the total number of samples
        """
        return len(self.samples)  # Hard Code

    def __getitem__(self, index):
        """
        Generate one sample of the dataset
        """

        sample = {}

        seq = self.samples[index]

        vidcap = cv2.VideoCapture(seq["full_path"])
        num_video_frames = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))



        if self.set_name == 'train':
            max_offset = max(0, num_video_frames - self.window_size)
            t = random.randint(0, max_offset)
        else:
            t = max(0, math.floor((num_video_frames - self.window_size) / 2))

        frame_indices = list(range(t, min(t + self.window_size, num_video_frames)))

        # tj : padding
        while len(frame_indices) < self.window_size:
            frame_indices.append(frame_indices[-1])

        rgbs = []



        for i in frame_indices:
            vidcap.set(cv2.CAP_PROP_POS_FRAMES, i)
            success, bgr = vidcap.read()
            # BGR (OpenCV) to RGB (Torch)
            if success:
                rgb = bgr[:, :, [2, 1, 0]]
                chw = np.transpose(rgb, (2, 0, 1))  # C*H*W
                rgbs.append(chw)
            else:
                return None

        # tchw = torch.stack(rgbs)
        tchw = np.stack(rgbs, axis=0)

        # # tj = : for temporaly see the tranformed results
        # for i in range(tchw.shape[0]):
        #     from torchvision.utils import save_image
        #     save_image(tchw[i], "image_{}.png".format(i))

        cthw = np.transpose(tchw, (1, 0, 2, 3))
        # cthw = tchw.permute(1, 0, 2, 3)


        sample['input'] = (cthw / 255).astype(np.float32) # tj : TCHW -> CTHW

        sample['cls'] = seq['label']
        sample['class_names'] = self.class_names
        sample['box'] = seq['box']
        sample['src_mask'] = np.ones((1+self.window_size), dtype=np.bool)
        return sample

