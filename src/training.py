import torch
from src.helpers import load_config, set_seed, make_model_dir, make_logger,\
    symlink_update, load_checkpoint, ConfigurationError, log_cfg, append_file, freeze_params, f1_score_numpy
from src.data import  make_data_loader, load_train_data, load_dev_data
from torch.utils import data
from src.model import *
from torch.utils.tensorboard import SummaryWriter
from src.builders import build_gradient_clipper, build_optimizer, build_scheduler
import numpy as np
import os
from torch.utils.data import Dataset
import time
from src.batch_with_mask import BatchWithMask, BatchWithMask, BatchWithMaskValid, BatchWithMaskValidCls, BatchWithMaskCls, MyBatch
from torch import Tensor
import queue
import shutil
from src.mask import gen_mask, gen_translated_mask, gen_joint_mask_option, gen_valid_mask
from src.loss import CE_Loss
from src.center_loss import CenterLoss
from src.prediction import validate
from src.my_resnet import *
from src.loss import TripletLoss
from src.draw import draw_ROC
import gc
from src.helpers import freeze_params
import torch.nn as nn
from src.i3d import *
from src.spatial_transforms import *
from src.temporal_transforms import *
from i3d_models.i3dpt_non import I3D_NON, Unit3Dpy
from i3d_models.i3dpt import I3D,Unit3Dpy
from src.vizutils import *
from src.performance import *
from src.averagemeter import *
from pathlib import Path
def memReport():
    for obj in gc.get_objects():
        if torch.is_tensor(obj):
            print(type(obj), obj.size())

class TrainManager:
    def __init__(self, model: nn.Module, config: dict) -> None:
        train_config = config["training"]
        data_config = config["data"]

        self.train_config = train_config
        self.data_config = data_config

        self.local_path = data_config['local_path']

        self.window_size = data_config["window_size"]



        self.save_last = train_config['save_last']


        self.with_confidence = train_config.get('with_confidence', False)

        self.with_sequence_ordering = data_config.get('with_sequence_ordering', False)

        # CPU / GPU, tj : this should be before build_optimizer, so that optimizer will know on what device to build
        self.use_cuda = train_config["use_cuda"]


        #objective]
        self.ce_loss = CE_Loss()
        # self.center_loss = CenterLoss(num_classes=1000, feat_dim=1024, use_gpu=self.use_cuda) # TODO: hard coding

        # model


        # self.loss.cuda()
        self.model = model
        if self.use_cuda:
            self.model.cuda()




        #optimization
        self.learning_rate_min = train_config.get("learning_rate_min", 1.0e-8)
        self.clip_grad_fun = build_gradient_clipper(config=train_config)
        self.optimizer = build_optimizer(config=train_config,
                                         parameters=model.parameters())

        # validation & early stopping
        self.validation_freq = train_config.get("validation_freq", 1000)
        self.max_validation_batches = train_config.get("max_validation_batches", 0)
        self.ckpt_queue = queue.Queue(
            maxsize=train_config.get("keep_last_ckpts", 5))
        self.eval_metric = train_config.get("eval_metric", "bleu")

        self.early_stopping_metric = train_config.get("early_stopping_metric",
                                                      "loss")

        # if we schedule after BLEU/chrf, we want to maximize it, else minimize
        # early_stopping_metric decides on how to find the early stopping point:
        # ckpts are written when there's a new high/low score for this metric
        if self.early_stopping_metric in ["ppl", "loss"]:
            self.minimize_metric = True
        elif self.early_stopping_metric in ["VAL", "acc", "prec"]:
            self.minimize_metric = False
        else:
            raise ConfigurationError(
                "Invalid setting for 'early_stopping_metric', "
                "valid options: 'loss', 'ppl'.")

        # learning rate sheduling
        self.scheduler, self.scheduler_step_at = build_scheduler(
            config=train_config,
            scheduler_mode="min" if self.minimize_metric else "max",
            optimizer=self.optimizer,
            hidden_size=config["model"]["encoder"]["hidden_size"])

        # data & batch handling
        self.shuffle = train_config.get("shuffle", True)
        self.max_epoch = train_config["max_epoch"]
        # self.epoch_size = train_config['epoch_size']
        self.batch_size = train_config["batch_size"]

        # self.class_per_batch = train_config["class_per_batch"]
        # self.vidoes_per_class = train_config["vidoes_per_class"]

        # batch_size = class_per_batch * vidoes_per_class

        self.eval_batch_size = train_config["eval_batch_size"]
        self.batch_multiplier = train_config.get("batch_multiplier", 1)

        self.alpha = train_config['alpha']


        self.mask_frames = train_config["mask_frames"]
        self.eval_mask_frames = train_config["eval_mask_frames"]


        self.masking_times = train_config["masking_times"]



        # initialize training statistics
        self.steps = 0
        # stop training if this flag is True by reaching learning rate minimum
        self.stop = False

        self.epoch = 0

        self.best_ckpt_iteration = 0
        # initial values for best scores
        self.best_ckpt_score = np.inf if self.minimize_metric else -np.inf
        # comparison function for scores
        self.is_best = lambda score: score < self.best_ckpt_score \
            if self.minimize_metric else score > self.best_ckpt_score


        self.model_dir, init = make_model_dir(train_config["model_dir"],  overwrite=train_config.get("overwrite"))
        self.logger = make_logger("{}/train.log".format(self.model_dir))


        if init is True:  # tj : init from last exit
            last_model = os.path.join(self.model_dir, "last.ckpt")
            best_model = os.path.join(self.model_dir, "best.ckpt")
            if os.path.exists(last_model):
                self.logger.info("init from last checkpoint {}".format(last_model))
                self.init_from_checkpoint(last_model)
            elif os.path.exists(best_model):
                self.logger.info("init from best checkpoint {}".format(best_model))
                self.init_from_checkpoint(best_model)
            else:
                print("no last or best model found! overwrite now")
                make_model_dir(train_config["model_dir"], overwrite=True)
                self._init_from_scratch()
        else: # tj : check shall we start based on other pre-trained model
            self._init_from_scratch()

        self.logging_freq = train_config.get("logging_freq", 100)
        self.valid_log_file = "{}/validations.log".format(self.model_dir)
        self.valid_report_file = "{}/validations.txt".format(self.model_dir)
        with open(self.valid_report_file, 'a') as opened_file:
            opened_file.write(
                "Epoch\tSteps\ttrain_Loss\ttrain_top1\ttrain_top5\t"
                "val_loss\tval_top1\tval_top5\tLR\n")


        self.tb_writer = SummaryWriter(log_dir=self.model_dir + "/tensorboard/")

        self._log_parameters_list()
        self.topk = [1, 5]
        self.show_figs = False

    def _init_from_scratch(self):
        if self.train_config.get("bsl1k_pretrained_model") is not None:
            self.logger.info("loading bsl1k pretrained model")
            self.init_cross_language = "asl_with_bsl"
            if self.local_path:
                self.word_data_pkl = "/home/seamanj/Workspace/bsl1k/misc/bsl1k/bsl1k_vocab.pkl"
            else:
                self.word_data_pkl = "/vol/research/extol/tmp/Tao/bsl1k/exp1/misc/bsl1k/bsl1k_vocab.pkl"

            self._load_checkpoint_flexible(self.train_config.get("bsl1k_pretrained_model"))
            # self.logger.info("load model from {}".format())
            # checkpoint = torch.load(train_config.get("bsl1k_pretrained_model"))
            # model.load_state_dict(checkpoint["state_dict"])
            #

        if "cnn_pretrained_model" in self.train_config.keys():
            self.logger.info("load model from {} {}".format(self.train_config["cnn_pretrained_model"]))
            pretrained_model_checkpoint = load_checkpoint(path=self.train_config["cnn_pretrained_model"],
                                                          use_cuda=self.use_cuda)
            self.model.cnn.load_state_dict(pretrained_model_checkpoint["cnn"])

        if "load_model" in self.train_config.keys():
            model_load_path = self.train_config["load_model"]
            self.logger.info("Loading model from %s", model_load_path)
            reset_epoch = self.train_config.get("reset_epoch", False)
            reset_steps = self.train_config.get("reset_steps", False)
            reset_best_ckpt = self.train_config.get("reset_best_ckpt", False)
            reset_scheduler = self.train_config.get("reset_scheduler", False)
            reset_optimizer = self.train_config.get("reset_optimizer", False)
            self.init_from_checkpoint(model_load_path,
                                      reset_epoch=reset_epoch,
                                      reset_steps=reset_steps,
                                      reset_best_ckpt=reset_best_ckpt,
                                      reset_scheduler=reset_scheduler,
                                      reset_optimizer=reset_optimizer)

    def _load_checkpoint_flexible(self, pretrained_path):
        msg = f"no pretrained model found at {pretrained_path}"
        assert Path(pretrained_path).exists(), msg
        self.logger.info(f"=> loading checkpoint '{pretrained_path}'")
        checkpoint = load_checkpoint(pretrained_path)

        # This part handles ignoring the last layer weights if there is mismatch
        partial_load = False
        if "state_dict" in checkpoint:
            pretrained_dict = checkpoint["state_dict"]
        else:
            self.logger.info("State_dict key not found, attempting to use the checkpoint:")
            pretrained_dict = checkpoint

        # If the pretrained model is not torch.nn.DataParallel(model), append 'module.' to keys.
        if "module" not in sorted(pretrained_dict.keys())[0]:
            self.logger.info('Appending "module." to pretrained keys.')
            pretrained_dict = dict(("module." + k, v) for (k, v) in pretrained_dict.items())

        model_dict = self.model.state_dict()

        for k, v in pretrained_dict.items():
            if not ((k in model_dict) and v.shape == model_dict[k].shape):
                self.logger.info(f"Unused from pretrain {k}")
                partial_load = True

        for k, v in model_dict.items():
            if k not in pretrained_dict:
                self.logger.info(f"Missing in pretrain {k}")
                partial_load = True

        if self.init_cross_language != "":
            # HACK: get the classifier weights before throwing them away
            from utils.cross_language import get_classification_params

            pretrained_w, pretrained_b = get_classification_params(
                pretrained_dict, arch="InceptionI3d"
            )

        if partial_load:
            self.logger.info("Removing or not initializing some layers...")
            # 1. filter out unnecessary keys
            pretrained_dict = {
                k: v
                for k, v in pretrained_dict.items()
                if (k in model_dict) and (v.shape == model_dict[k].shape)
            }

            # 2. overwrite entries in the existing state dict
            model_dict.update(pretrained_dict)
            # 3. load the new state dict
            self.model.load_state_dict(model_dict)
            # CAUTION: Optimizer not initialized with the pretrained one

            # Modification TO MAKE THE BSL ASL correspond
            if self.init_cross_language != "":
                from utils.cross_language import init_cross_language

                # NOTE: Update from Samuel that preserves previous behaviour if asl_dataset
                # is not set
                asl_dataset = "msasl"

                self.model = init_cross_language(
                    init_str=self.init_cross_language,
                    model=self.model,
                    pretrained_w=pretrained_w,
                    pretrained_b=pretrained_b,
                    asl_dataset=asl_dataset,
                    bsl_pkl=self.word_data_pkl,
                    local_path=self.local_path
                )
        else:
            self.logger.info("Loading state dict.")
            self.model.load_state_dict(checkpoint["state_dict"])
            self.logger.info("Loading optimizer.")
            self.optimizer.load_state_dict(checkpoint["optimizer"])

        del checkpoint, pretrained_dict
        # if args.featurize_mode:
        #     assert not partial_load, "Must use full weights for featurization!"
        return partial_load

    def _save_checkpoint(self) -> None:
        model_path = "{}/{}.ckpt".format(self.model_dir, self.steps)
        state = {
            "epoch": self.epoch,
            "steps": self.steps,
            "best_ckpt_score": self.best_ckpt_score,
            "best_ckpt_iteration": self.best_ckpt_iteration,
            "model_state": self.model.module.state_dict(),
            # "cnn": self.model.cnn.state_dict(),
            # "pose_regressor": self.model.pose_regressor.state_dict(),
            # "embed": self.model.embed.state_dict(),
            # "encoder": self.model.encoder.state_dict(),
            # "skeleton_output_layer": self.model.skeleton_output_layer.state_dict(),
            # "pre_classifier": self.model.pre_classifier.state_dict(),
            # "cls_output_layer": self.model.cls_output_layer.state_dict(),
            "optimizer_state": self.optimizer.state_dict(),
            "scheduler_state": self.scheduler.state_dict() if \
            self.scheduler is not None else None,
        }
        torch.save(state, model_path)
        if self.ckpt_queue.full():
            to_delete = self.ckpt_queue.get()  # delete oldest ckpt
            try:
                os.remove(to_delete)
            except FileNotFoundError:
                self.logger.warning("Wanted to delete old checkpoint %s but "
                                    "file does not exist.", to_delete)

        self.ckpt_queue.put(model_path)

        best_path = "{}/best.ckpt".format(self.model_dir)
        try:
            # create/modify symbolic link for best checkpoint
            symlink_update("{}.ckpt".format(self.steps), best_path)
        except OSError:
            # overwrite best.ckpt
            torch.save(state, best_path)

    def _save_last_checkpoint(self) -> None:
        model_path = "{}/last.ckpt".format(self.model_dir)
        state = {
            "epoch": self.epoch,
            "steps": self.steps,
            "best_ckpt_score": self.best_ckpt_score,
            "best_ckpt_iteration": self.best_ckpt_iteration,
            "model_state": self.model.state_dict(),
            # "cnn": self.model.cnn.state_dict(),
            # "pose_regressor": self.model.pose_regressor.state_dict(),
            # "embed": self.model.embed.state_dict(),
            # "encoder": self.model.encoder.state_dict(),
            # "skeleton_output_layer": self.model.skeleton_output_layer.state_dict(),
            # "pre_classifier": self.model.pre_classifier.state_dict(),
            # "cls_output_layer": self.model.cls_output_layer.state_dict(),
            "optimizer_state": self.optimizer.state_dict(),
            "scheduler_state": self.scheduler.state_dict() if \
            self.scheduler is not None else None,
        }
        torch.save(state, model_path)


    def init_from_checkpoint(self, path: str,
                             reset_epoch: bool = False,
                             reset_steps: bool = False,
                             reset_best_ckpt: bool = False,
                             reset_scheduler: bool = False,
                             reset_optimizer: bool = False) -> None:

        model_checkpoint = load_checkpoint(path=path, use_cuda=self.use_cuda)

        # restore model and optimizer parameters
        # self.model.cnn.load_state_dict(model_checkpoint["cnn"])
        # self.model.embed.load_state_dict(model_checkpoint["embed"])
        # self.model.encoder.load_state_dict(model_checkpoint["encoder"])
        # # self.model.skeleton_output_layer.load_state_dict(model_checkpoint["skeleton_output_layer"])
        # self.model.pre_classifier.load_state_dict(model_checkpoint['pre_classifier'])
        # self.model.cls_output_layer.load_state_dict(model_checkpoint['cls_output_layer'])

        self.model.load_state_dict(model_checkpoint['model_state'])

        # tj : freeze the params
        # freeze_params(self.model.embed)
        # freeze_params(self.model.encoder)
        # freeze_params(self.model.pre_classifier)

        # self.model.cls_output_layer.load_state_dict(model_checkpoint['cls_output_layer'])
        if not reset_optimizer:
            self.optimizer.load_state_dict(model_checkpoint["optimizer_state"])
        else:
            self.logger.info("Reset optimizer.")

        if not reset_scheduler:
            if model_checkpoint["scheduler_state"] is not None and \
                    self.scheduler is not None:
                self.scheduler.load_state_dict(
                    model_checkpoint["scheduler_state"])
        else:
            self.logger.info("Reset scheduler.")

        # restore counts

        if not reset_epoch:
            self.epoch = model_checkpoint["epoch"] + 1
        else:
            self.logger.info("Reset epoch.")

        if not reset_steps:
            self.steps = model_checkpoint["steps"]
        else:
            self.logger.info("Reset steps.")

        if not reset_best_ckpt:
            self.best_ckpt_score = model_checkpoint["best_ckpt_score"]
            self.best_ckpt_iteration = model_checkpoint["best_ckpt_iteration"]
        else:
            self.logger.info("Reset tracking of the best checkpoint.")

        # move parameters to cuda
        if self.use_cuda:
            self.model.cuda()



    def _log_parameters_list(self) -> None:
        model_parameters = filter(lambda p: p.requires_grad,
                                  self.model.parameters())
        n_params = sum([np.prod(p.size()) for p in model_parameters])
        self.logger.info("Total params: %d", n_params)
        trainable_params = [n for (n, p) in self.model.named_parameters()
                            if p.requires_grad]
        self.logger.info("Trainable prameters: %s", sorted(trainable_params))
        assert trainable_params


    def do_epoch(self, set_name):
        assert set_name == 'train' or set_name == 'val'
        losses = [AverageMeter()]
        perfs = []
        for k in self.topk:
            perfs.append(AverageMeter())

        loader = None
        if set_name == 'train':
            self.model.train()
            loader = self.train_loader
        elif set_name == 'val':
            self.model.eval()
            loader = self.valid_loader
        gt_win, pred_win, fig_gt_pred = None, None, None
        for i, batch in enumerate(loader):
            if batch is None:  # tj : filter out None in my_collate
                continue
            with torch.no_grad():
                batch["input"] = batch["input"].cuda()
                collater_kwargs = {}
                collater = loader.dataset.gpu_collater
                batch = collater(minibatch=batch, **collater_kwargs)  # tj: 256 -> 224

            inputs = batch['input']
            targets = batch['cls']
            masks = batch['src_mask']

            inputs_cuda = inputs.cuda()
            targets_cuda = targets.cuda()
            masks_cuda = masks.cuda()

            outputs_cuda = self.model(inputs_cuda, self.window_size, masks_cuda)

            # conpute the loss
            logits = outputs_cuda['logits'].data.cpu()
            loss = self.ce_loss(outputs_cuda['logits'], targets_cuda)
            topk_acc = performance(logits, targets, )
            for ki, acc in enumerate(topk_acc):
                perfs[ki].update(acc, inputs.size(0))

            losses[0].update(loss.item(), inputs.size(0))

            if set_name == 'train': # tj : # log training progress
                if self.scheduler is not None and \
                        self.scheduler_step_at == "step":
                    self.scheduler.step()

                if self.steps % self.logging_freq == 0:
                    self.logger.info(
                        "Epoch %3d Step: %8d Batch Loss: %12.6f Lr: %12.8f",
                        self.epoch, self.steps, float(losses[0].avg), self.optimizer.param_groups[0]["lr"])

            num_figs = 10
            if ( self.show_figs and is_show(num_figs, i, len(loader))):
                fname = "pred_%s_epoch%02d_iter%05d" % (set_name, self.epoch, i)
                save_fig_dir = os.path.join(self.model_dir, "figs")
                save_path = os.path.join(save_fig_dir, fname)
                mean = torch.Tensor([0.5, 0.5, 0.5])
                std = torch.Tensor([1.0, 1.0, 1.0])
                gt_win, pred_win, fig_gt_pred = viz_gt_pred(
                    batch['input'],
                    outputs_cuda['logits'].cpu(),
                    batch['cls'],
                    mean,
                    std,
                    batch,
                    gt_win,
                    pred_win,
                    fig_gt_pred,
                    save_path=save_path,
                    show=False,
                )

            if set_name == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                if self.clip_grad_fun is not None:
                    # clip gradients (in-place)
                    self.clip_grad_fun(params=self.model.parameters())

                self.steps += 1

        return losses, perfs





    def train_and_validate(self, train_data: Dataset, valid_data: Dataset) -> None:
        self.train_loader = make_data_loader(train_data,
                                        batch_size=self.batch_size,
                                        shuffle=self.shuffle)

        self.valid_loader = make_data_loader(dataset=valid_data,
                                        batch_size=self.eval_batch_size,
                                        shuffle=False)

        # for epoch_no in range(self.epochs):
        while self.epoch < self.max_epoch:
            self.logger.info("EPOCH %d", self.epoch)

            if self.scheduler is not None and self.scheduler_step_at == "epoch":
                self.scheduler.step(epoch=self.epoch)

            train_losses, train_perfs = self.do_epoch('train')
            valid_losses, valid_perfs = self.do_epoch('val')

            # tj : log

            train_loss = float(train_losses[0].avg) # tj : only one loss
            train_top1 = float(train_perfs[0].avg)
            train_top5 = float(train_perfs[1].avg)

            valid_loss = float(valid_losses[0].avg)
            valid_top1 = float(valid_perfs[0].avg)
            valid_top5 = float(valid_perfs[1].avg)



            self.tb_writer.add_scalar("train/loss", train_loss, self.epoch)
            self.tb_writer.add_scalar("train/top1", train_top1, self.epoch)
            self.tb_writer.add_scalar("train/top5", train_top5, self.epoch)

            self.tb_writer.add_scalar("val/loss", valid_loss, self.epoch)
            self.tb_writer.add_scalar("val/top1", valid_top1, self.epoch)
            self.tb_writer.add_scalar("val/top5", valid_top5, self.epoch)


            # tj : save checkpoint
            if self.early_stopping_metric == "loss":
                ckpt_score = valid_loss
            elif self.early_stopping_metric == "prec":
                ckpt_score = valid_top1
            else:
                raise ConfigurationError("Nothing else supported yet!")

            new_best = False
            if self.is_best(ckpt_score):
                self.best_ckpt_score = ckpt_score
                self.best_ckpt_iteration = self.steps
                self.logger.info(
                    'Hooray! New best validation result [%s]!',
                    self.early_stopping_metric)
                if self.ckpt_queue.maxsize > 0:
                    self.logger.info("Saving new checkpoint.")
                    new_best = True
                    self._save_checkpoint()

            if self.save_last:
                self.logger.info("Saving the last checkpoint")
                self._save_last_checkpoint()

            # tj : as we validate every epoch here, so scheduler_step_at == 'validation' equals to scheduler_step_at == 'epoch'
            if self.scheduler is not None \
                    and self.scheduler_step_at == "validation":
                self.scheduler.step(ckpt_score)

            self._add_report(
                train_loss=train_loss,
                train_top1=train_top1,
                train_top5=train_top5,
                val_loss=valid_loss,
                val_top1=valid_top1,
                val_top5=valid_top5,
                new_best=new_best)

            # tj : check whether to stop or not
            if self.stop:
                self.logger.info(
                    'Training ended since minimum lr %12.8f was reached.',
                    self.learning_rate_min
                )
                self.logger.info('Training ended after %3d epochs.', self.epoch)
                break

            self.epoch += 1

        self.tb_writer.close()

    def _add_report(self, train_loss: float, train_top1: float, train_top5: float, val_loss: float, val_top1: float,
                    val_top5: float, new_best: bool = False) -> None:
        """
        Append a one-line report to validation logging file.

        :param valid_score: validation evaluation score [eval_metric]
        :param valid_loss: validation loss (sum over whole validation set)
        :param eval_metric: evaluation metric, e.g. "bleu"
        :param new_best: whether this is a new best model
        """
        current_lr = -1
        # ignores other param groups for now
        for param_group in self.optimizer.param_groups:
            current_lr = param_group['lr']

        if current_lr < self.learning_rate_min:
            self.stop = True

        with open(self.valid_report_file, 'a') as opened_file:
            opened_file.write(
                "{:<5}\t{:<5}\t{:<10.3f}\t{:<10.3f}\t{:<10.3f}\t"
                "{:<8.3f}\t{:<8.3f}\t{:<8.3f}\t{:.8f}\t{}\n".format(
                    self.epoch, self.steps, train_loss, train_top1, train_top5,
                    val_loss, val_top1, val_top5, current_lr, "*" if new_best else ""))



def train(cfg_file: str) -> None:

    cfg = load_config(cfg_file)

    set_seed(seed=cfg["training"].get("random_seed", 42))

    #training_data, dev_data, test_data = load_data(data_cfg=cfg["data"])

    scales = [cfg['data']['initial_scale']]
    for i in range(1, cfg['data']['n_scales']):
        scales.append(scales[-1] * cfg['data']['scale_step'])


    # tj = : frame level
    spatial_transform = Compose([
        # crop_method,
        ToTensor(255)
    ])

    temporal_transform = LastPadding(cfg['data']['window_size'])


    # tj = : video level
    spatiotemporal_transform = Compose([
        torchvision.transforms.Resize((256, 256), interpolation=2),
        torchvision.transforms.RandomAffine(degrees=0, scale=(0.9,1.1)),
        RandomScale(256, 0.1),
        torchvision.transforms.RandomCrop(224),
        torchvision.transforms.RandomHorizontalFlip(p=0.5),
        # ColorJitter(num_in_frames=cfg['data']['window_size'], thr=0.2),
        torchvision.transforms.ColorJitter(
            brightness=0.2,
            contrast=0.2,
            saturation=0.2,
            hue=0.2),
        torchvision.transforms.Normalize(0.5 * torch.ones(3), 1.0 * torch.ones(3))
        ])

    # spatiotemporal_transform = None

    training_data = load_train_data(data_cfg=cfg["data"], spatial_transform=spatial_transform,
                                    temporal_transform=temporal_transform, spatiotemporal_transform=spatiotemporal_transform)

    spatiotemporal_transform = Compose([
        torchvision.transforms.Resize((256, 256), interpolation=2),
        torchvision.transforms.CenterCrop(224),
        torchvision.transforms.Normalize(0.5 * torch.ones(3), 1.0 * torch.ones(3))
        ])

    dev_data = load_dev_data(data_cfg=cfg["data"], spatial_transform=spatial_transform, temporal_transform=temporal_transform,  spatiotemporal_transform=spatiotemporal_transform)

    model = build_model(cfg["model"], resnet18())  # tj : build our model

    # model = I3D(num_classes=1000, modality='rgb', dropout_prob=0)
    # model.conv3d_0c_1x1 = Unit3Dpy(
    #     in_channels=1024,
    #     out_channels=1000,
    #     kernel_size=(1, 1, 1),
    #     activation=None,
    #     use_bias=True,
    #     use_bn=False)

    # model = InceptionI3d(num_classes=1000,
    #                      spatiotemporal_squeeze=True,
    #                      final_endpoint='Logits',
    #                      name='inception_i3d',
    #                      in_channels=3,
    #                      dropout_keep_prob=0.5,
    #                      num_in_frames=cfg['data']['window_size'],
    #                      include_embds=True)

    model = torch.nn.DataParallel(model).cuda()

    trainer = TrainManager(model=model, config=cfg)  # tj : all the setting pass to its member

    trainer.logger.info("train frames:{}".format(training_data.__len__()))
    trainer.logger.info("dev frames:{}".format(dev_data.__len__()))


    shutil.copy2(cfg_file, trainer.model_dir + "/config.yaml")

    # log all entries of config
    log_cfg(cfg, trainer.logger)

    #TODO : log data info

    trainer.logger.info(str(model))

    trainer.train_and_validate(train_data=training_data, valid_data=dev_data)

