# coding: utf-8
"""
Module to represents whole models
"""

import numpy as np

import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.functional as F

#from src.initialization import initialize_model
from src.embeddings import Embeddings
from src.encoders import *
from src.batch_with_mask import BatchWithMask, BatchWithMask, BatchWithMaskValid, BatchWithMaskValidCls, BatchWithMaskCls, MyBatch
from src.initialization import initialize_model
from src.noise import make_noise
import torch.nn.functional as f

import torchvision
from src.my_resnet import *
from src.helpers import *

class SignNet(nn.Module):
    """
    Base Model class
    """

    def __init__(self,
                 cnn: ResNet,
                 encoder: Encoder,
                 embed: Embeddings,
                 hidden_size,
                 img_feature_dim
                 ) -> None:

        super(SignNet, self).__init__()

        self.hidden_size = hidden_size
        self.img_feature_dim = img_feature_dim
        self.cls_token = torch.zeros(1, self.img_feature_dim, dtype=torch.float32).cuda()
        self.cls_token[0, 0] = 1

        self.cnn = cnn
        self.pose_regressor = nn.Linear(self.cnn.feature_dim, 274)
        self.embed = embed
        self.encoder = encoder
        self.skeleton_output_layer = nn.Linear(self.encoder.output_size, img_feature_dim, bias=False)

        self.output_feature_dim = 1024
        self.num_classes = 1000
        self.pre_classifier = nn.Linear(self.encoder.output_size, self.output_feature_dim)

        # self.cls_output_layer = nn.Sequential(nn.Linear(self.encoder.output_size, self.num_classes),
        #                                       nn.Softmax(dim=-1))

        self.cls_output_layer = nn.Linear(self.encoder.output_size, self.num_classes)
        self.dropout = nn.Dropout(p=0) # tj : note dropout has no params to save



    # pylint: disable=arguments-differ

    def forward(self, src: Tensor, src_length: Tensor, src_mask: Tensor):  # tj : src_mask is for attention score, feature_mask is for masking source feature
        """
        First encodes the source sentence.
        Then produces the target one word at a time.

        :param src: source input
        :param trg_input: target input
        :param src_mask: source mask
        :param src_lengths: length of source inputs
        :param trg_mask: target mask
        :return: decoder outputs
        """
        btchw = src.permute(0, 2, 1, 3, 4) # tj : torch.Size([4, 64, 3, 224, 224])
        nchw = btchw.contiguous().view([-1] + list(btchw.shape[-3:])) # tj : torch.Size([256, 3, 224, 224])

        img_feature = self.cnn(nchw) # tj : torch.Size([256, 512])

        btf = img_feature.view(list(btchw.shape[:2]) + [-1]) # tj : torch.Size([4, 64, 512])
        cls_tokens = self.cls_token.repeat(btchw.shape[0], 1, 1)
        img_feature = torch.cat((cls_tokens, btf), 1)

        encoded, _ = self.encode(src=img_feature, src_length=src_length, src_mask=src_mask) # tj : torch.Size([4, 64, 1024])
        # # skeleton_output = torch.matmul(encoded, self.embed.emb.transpose(0, 1))
        # skeleton_output = self.skeleton_output_layer(encoded) # tj : 64 * 128 * 512 -> 64 * 128 * 144
        # encoded_flattened = encoded.flatten(start_dim = 1)
        # feature_output = self.dropout(self.pre_classifier(encoded))
        # feature_output = f.normalize(feature_output, dim=1, p=2)
        feature_output = encoded[:,0,:]
        # pooled_cls_out = nn.ReLU()(pooled_cls_out)
        # feature_output = encoded
        logits = self.cls_output_layer(feature_output)
        # logits = torch.mean(logits, dim=1)
        # return encoded, feature_output, logits

        return {"encoded": encoded,
                "feature_output": encoded,
                "logits": logits}



    def encode(self, src: Tensor, src_length: Tensor, src_mask: Tensor) :

        """
        Encodes the source sentence.

        :param src:
        :param src_length:
        :param src_mask:
        :return: encoder outputs (output, hidden_concat)
        """

        # = tj: apply the mask
        # masked_src = src.clone()

        embed_skel = self.embed(x=src)

        return self.encoder(embed_src=embed_skel, src_length=src_length, mask=src_mask)






    def __repr__(self) -> str:
        """
        String representation: a description of encoder, decoder and embeddings

        :return: string representation
        """
        return "%s(\n" \
               "\tencoder=%s,\n" \
               "\tsrc_embed=%s,\n" \
               % (self.__class__.__name__, self.encoder,
                  self.embed)


def build_model(cfg: dict,
                cnn: ResNet) -> SignNet:
    """
    Build and initialize the model according to the configuration.

    :param cfg: dictionary configuration containing model specifications
    :param src_vocab: source vocabulary
    :param trg_vocab: target vocabulary
    :return: built and initialized model
    """

    img_feature_dim = cnn.feature_dim


    embedding_dim = cfg["encoder"]["embeddings"]["embedding_dim"]
    freeze = cfg["encoder"]["embeddings"]["freeze"]
    embed = Embeddings(
        embedding_dim=embedding_dim,
        feature_dim=img_feature_dim,
        freeze=freeze)


    # build encoder
    with Block("Encoder"):

        hidden_size = cfg["encoder"]["hidden_size"]
        ff_size = cfg["encoder"]["ff_size"]
        num_layers = cfg["encoder"]["num_layers"]
        num_heads = cfg["encoder"]["num_heads"]
        dropout = cfg["encoder"]["dropout"]
        emb_dropout = cfg["encoder"]["embeddings"]["emb_dropout"]
        freeze = cfg["encoder"]["freeze"]


        if cfg["encoder"]["type"] == "transformer":
            assert cfg["encoder"]["embeddings"]["embedding_dim"] == \
                   cfg["encoder"]["hidden_size"], \
                "for transformer, emb_size must be hidden_size"

            encoder = TransformerEncoder(hidden_size=hidden_size,
                                         ff_size=ff_size,
                                         num_layers=num_layers,
                                         num_heads=num_heads,
                                         dropout=dropout,
                                         emb_dropout=emb_dropout,
                                         freeze=freeze)
        else:
            encoder = RecurrentEncoder(**cfg["encoder"],
                                       emb_size=embed.embedding_dim,
                                       emb_dropout=emb_dropout)




    model = SignNet(cnn=cnn, encoder=encoder, embed=embed,
                  hidden_size=cfg["encoder"]["hidden_size"],
                  img_feature_dim=img_feature_dim)

    # custom initialization of model parameters
    initialize_model(model, cfg)

    return model
